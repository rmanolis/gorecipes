package confs

import (
	"log"
	"net/http"
	"os"
	"time"

	"github.com/codegangsta/negroni"
)

type loggers struct {
	Info  *log.Logger
	Error *log.Logger
}

var LOG = loggers{
	Info:  log.New(os.Stdout, "INFO: ", log.LstdFlags|log.Llongfile),
	Error: log.New(os.Stderr, "ERROR: ", log.LstdFlags|log.Llongfile),
}

type HttpLogger struct {
	*log.Logger
}

// NewLogger returns a new Logger instance
func NewLogger() *HttpLogger {
	return &HttpLogger{log.New(os.Stdout, "[gorecipes] ", 0)}
}

func (l *HttpLogger) ServeHTTP(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	start := time.Now()
	l.Printf("Started %s %s %s", r.RemoteAddr, r.Method, r.URL.Path)
	l.Printf("UserData %s", r.Header.Get("User-Agent"))
	next(rw, r)

	res := rw.(negroni.ResponseWriter)
	l.Printf("Completed %v %s in %v", res.Status(), http.StatusText(res.Status()), time.Since(start))
}
