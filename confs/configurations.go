package confs

import (
	"github.com/unrolled/render"
)

const DBName = "beegorecipes"

var Render = render.New(render.Options{
	Directory:     "views",
	Layout:        "base",
	Delims:        render.Delims{"{%", "%}"},
	Extensions:    []string{".tpl"},
	UnEscapeHTML:  true,
	IsDevelopment: true,
})

var NoLayoutRender = render.New(render.Options{
	Directory:     "views",
	Delims:        render.Delims{"{%", "%}"},
	Extensions:    []string{".tpl"},
	UnEscapeHTML:  true,
	IsDevelopment: true,
})
