#!/bin/sh

go get gopkg.in/mgo.v2
go get github.com/gorilla/context
go get github.com/gorilla/mux
go get github.com/codegangsta/negroni
go get github.com/goincremental/negroni-sessions
go get github.com/rakyll/magicmime
go get github.com/rs/cors
go get github.com/unrolled/render

