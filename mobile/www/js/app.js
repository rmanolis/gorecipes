var app = angular.module('starter', ['ionic',  'ion-autocomplete','ngCordova'])
app.config(function($httpProvider, $stateProvider, $urlRouterProvider){
  $httpProvider.defaults.withCredentials = true;
  $urlRouterProvider.otherwise('/')

    $stateProvider.state('recipes', {
      url: '/',
      templateUrl: 'js/pages/search_recipes.html',
       cache: false,
      controller:"SearchRecipesCtrl"
    }).state('login', {
      url: '/login',
      templateUrl: 'js/pages/login.html',
       cache: false,
      controller:"LoginCtrl"
    }).state('groceries', {
      url: '/groceries',
      templateUrl: 'js/pages/current_groceries.html',
       cache: false,
      controller:"CurrentGroceriesCtrl"
    }).state('saved_groceries', {
      url: '/saved_groceries/:id/:name',
      templateUrl: 'js/pages/saved_groceries.html',
       cache: false,
      controller:"SavedGroceriesCtrl"
    }).state('saved_grocery_lists', {
      url: '/saved_grocery_lists',
      templateUrl: 'js/pages/saved_grocery_lists.html',
       cache: false,
      controller:"SavedGroceryListsCtrl"
    }).state('product', {
      url: '/product/:id',
      templateUrl: 'js/pages/recipe.html',
       cache: false,
      controller:"ProductCtrl"
    }).state('recipe', {
      url: '/recipe/:id',
      templateUrl: 'js/pages/recipe.html',
       cache: false,
      controller:"RecipeCtrl"
    })



});
app.run(function($ionicPlatform,  $log, CookieSrv) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }

  });
  $log.info("A");  
  CookieSrv.newCookie();

});
