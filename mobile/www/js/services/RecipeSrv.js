app.factory('RecipeSrv',
    ["$http","ConfSrv",
    function($http, ConfSrv){
  var obj = {};
   obj.getRecipes = function(name){
    return $http({
      method: "GET",
      url: ConfSrv.api + "/rest/recipes?" + "Name="+name,
    });
  }

  obj.getRecipe = function(id){
    return $http({
      method: "GET",
      url: ConfSrv.api + "/rest/recipes/"+id
    }) 
  }


  obj.searchRecipes = function(name, tags, ingredients, page, size){
    return $http({
      method: "POST",
      url: ConfSrv.api +"/rest/search/recipes?p="+ page + "&s=" + size ,
      data: {
        "Name": name ,
        "Tags": tags,
        "Ingredients": ingredients,
      }
    })

  }

  obj.getChoosenRecipes =  function(){
    return $http({
      method: "GET",
      url: ConfSrv.api + "/rest/choosen/recipes",
    })

  }
  obj.chooseRecipe = function(id){
    return $http({
      method: "POST",
      url: ConfSrv.api +"/rest/choosen/recipes/" + id,
    })
  }

  obj.rejectRecipe = function(id){
    return $http({
      method: "DELETE",
      url: ConfSrv.api + "/rest/choosen/recipes/" + id ,
    })
  }

  return obj;
    }]);
