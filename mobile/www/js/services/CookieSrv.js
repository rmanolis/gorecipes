app.factory('CookieSrv',
    ["$http", "ConfSrv",
    function($http, ConfSrv){
  var obj = {};
  obj.saveCookie = function (name) {
    return $http({
      method: "PUT",
      url: ConfSrv.api + "/rest/cookie/save",
      data:{
        Name:name,
      },
    });
  }

  obj.cancelCookie = function ( ) {
    return $http({
      method: "PUT",
      url: ConfSrv.api + "/rest/cookie/cancel",
    });
  }


  obj.newCookie = function(){
    return $http({
      method: "POST",
      url: ConfSrv.api + "/rest/cookie",
    });

  }

  return obj; 
    }])
