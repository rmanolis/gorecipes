app.factory('UserSrv',
    ["$http", "ConfSrv",
    function($http, ConfSrv){
      var obj = {};
      obj.isUser = function(){
        return $http({
          method: "GET",
          url: ConfSrv.api +"/rest/auth/user",    
        });

      }
 
      obj.login = function(email, password){
        return $http({
          method: "POST",
          url: ConfSrv.api + "/rest/auth/login", 
          data:{
            Email: email,
            Password: password,
          }
        });
      }

      obj.logout = function(email, password){
        return $http({
          method: "GET",
          url: ConfSrv.api + "/rest/auth/logout", 
        });
      }

      obj.getGroceryLists = function(){
        return $http({
          method: "GET",
          url: ConfSrv.api +"/rest/user/grocery_lists",    
        });
      }

      obj.getGroceryRecipes = function(cookie_id){
        return $http({
          method: "GET",
          url: ConfSrv.api +"/rest/user/grocery_lists/" + cookie_id +"/recipes",    
        });
      }

      obj.getGroceryProducts = function(cookie_id){
        return $http({
          method: "GET",
          url: ConfSrv.api +"/rest/user/grocery_lists/" + cookie_id +"/products",    
        });
      }

      obj.getGroceryIngredients = function(cookie_id){
        return $http({
          method: "GET",
          url: ConfSrv.api +"/rest/user/grocery_lists/" + cookie_id +"/ingredients",    
        });
      }

      obj.deleteGrocery = function(cookie_id){
        return $http({
          method: "DELETE",
          url: ConfSrv.api +"/rest/user/grocery_lists/" + cookie_id
        });
      }

      return obj;
    }]);
