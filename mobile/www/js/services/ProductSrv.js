app.factory('ProductSrv',
    ["$http", "ConfSrv",
    function($http, ConfSrv){
  var obj = {};

obj.getProducts = function(name){
    return $http({
      method: "GET",
      url: ConfSrv.api + "/rest/products?" + "Name="+name,
    });
  }

  obj.getProduct = function(id){
    return $http({
      method: "GET",
      url: ConfSrv.api + "/rest/products/"+id
    }) 
  }

  
   obj.searchProducts = function(name, tags, ingredients, page, size){
    return $http({
      method: "POST",
      url: ConfSrv.api + "/rest/search/products?p="+ page + "&s="+size,
      data: {
        "Name": name ,
        "Tags": tags,
        "Ingredients": ingredients,
      }
    })

  }


  obj.getProductsByChoosenRecipes = function(){
    return $http({
      method: "GET",
      url:  ConfSrv.api + "/rest/choosen/recipes/products",
    })
  }

  obj.getProductsByIngredient = function(id){
    return $http({
      method: "GET",
      url: ConfSrv.api + "/rest/ingredients/"+ id +"/products",
    })
  }

  obj.getChoosenProducts =  function(){
    return $http({
      method: "GET",
      url: ConfSrv.api + "/rest/choosen/products",
    })

  }

  obj.chooseProduct = function(id){
    return $.ajax({
      method: "POST",
      url: ConfSrv.api +"/rest/choosen/products/" + id,
    })
  }

  obj.rejectProduct = function(id){
    return $.ajax({
      method: "DELETE",
      url:ConfSrv.api + "/rest/choosen/products/" + id ,
    })
  }



  return obj;
    }]);
