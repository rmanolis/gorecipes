app.factory('TagSrv',
    ["$http", "ConfSrv",
    function($http, ConfSrv){
      var obj = {};

      obj.getTags = function(name){
        return $http({
          method: "GET",
          url: ConfSrv.api + "/rest/tags?" + "Name="+name,
        });
      }

      obj.getTag = function(id){
        return $http({
          method: "GET",
          url: ConfSrv.api +"/rest/tags/" + id,
        });
      }

      obj.searchTags = function(name){
        return $http({
          method: "POST",
          url: ConfSrv.api +"/rest/search/tags",
          data:{
            Name:name,
          }
        });

      }

      return obj;
    }]);
