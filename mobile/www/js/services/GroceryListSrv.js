app.factory('GroceryListSrv',
    ["$http","stor", "$log",
    function($http, stor  ,  $log){
      var obj = {};
      /*
       * GroceryList{
       *  Ingredients: [
       *    {Name: string,
       *     State: string
       *     }
       *  ]
       *  Recipes: [{
       *    Name: string
       *    Id : string
       *  }]
       *  Products : [{
       *    Name: string
       *    Id : string
       *  }]
       * }
       *
       */

      var col = stor.db.collection("groceries");
      col.load(function(err){
        if(err){
          $log.error(err);
        }
      });


      obj.addGroceryList = function(note){
        col.insert(note);
        col.save();
      }


      obj.getGroceryLists = function(){
        return col.find();
      }

      obj.getGroceryList = function(id){
        return col.find({Id:id});
      }

      obj.deleteGroceryList = function(id){
        col.remove({Id:id});
        col.save();
      }

      obj.editGroceryList = function(id, note){
        col.update({Id:id}, note);
        col.save();
      }

      return obj;

    }])
