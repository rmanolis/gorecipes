app.factory('IngredientSrv',
    ["$http", "ConfSrv",
    function($http, ConfSrv){
  var obj = {};
  
  obj.getIngredients = function(name){
    return $http({
      method: "GET",
      url: ConfSrv.api + "/rest/ingredients?" + "Name="+name,
    });
  }


  obj.searchIngredients = function(name, tags, page, size){
    return $http({
      method: "POST",
      url: ConfSrv.api + "/rest/search/ingredients?p="+ page + "&s="+size,
      data: {
        "Name": name ,
        "Tags": tags,
      }
    })

  }


  obj.getIngredient = function(id){
    return $http({
      method: "GET",
      url: ConfSrv.api + "/rest/ingredients/"+id
    }) 
  }

  obj.getIngredientsByChoosenRecipes = function(){
    return $http({
      method: "GET",
      url: ConfSrv.api + "/rest/choosen/recipes/ingredients",
    })
  }

  obj.getIngredientsByRecipe = function(id){
    return $http({
      method: "GET",
      url: ConfSrv.api + "/rest/recipes/"+id+"/ingredients",
    })
  }


  return obj;
    }]);
