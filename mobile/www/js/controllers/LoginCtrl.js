app.controller('LoginCtrl', 
    ["$scope", "$state","UserSrv",
    function($scope, $state, UserSrv) {
      
      $scope.submit = function(email, password){
       UserSrv.login(email,password).success(function(){
          $state.go("recipes");
       })
      }

    }]);

