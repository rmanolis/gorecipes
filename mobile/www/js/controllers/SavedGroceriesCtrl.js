app.controller('SavedGroceriesCtrl', 
    ["$scope", "$stateParams", "UserSrv","GroceryListSrv",
    function($scope,$stateParams,UserSrv,GroceryListSrv) {
      var cookie_id = $stateParams.id;
      $scope.note ={};
      var notes = GroceryListSrv.getGroceryList(cookie_id);
      angular.forEach(notes, function(note){
        console.log(note);
        $scope.note = note;
       
      })

      $scope.updateIngredients = function(){
        GroceryListSrv.editGroceryList($scope.note.Id,
            {Ingredients:$scope.note.Ingredients});
      }



    }]);

