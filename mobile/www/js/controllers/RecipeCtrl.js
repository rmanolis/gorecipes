app.controller('RecipeCtrl', 
    ["$scope", "$stateParams", "RecipeSrv",
    function($scope, $stateParams, RecipeSrv) {
      $scope.recipe = {};
      RecipeSrv.getRecipe($stateParams.id).success(function(recipe){
        $scope.recipe = recipe;
      });

    $scope.choose = function(){
     RecipeSrv.chooseRecipe($scope.recipe.Id).success(function(){
      $scope.recipe.IsChoosen = true; 
     })
    }

    $scope.reject = function(){
       RecipeSrv.rejectRecipe($scope.recipe.Id).success(function(){
        $scope.recipe.IsChoosen = false; 
       })
    }

    }]);
