app.controller('CurrentGroceriesCtrl', 
    ["$scope","$state","$ionicModal", "UserSrv","RecipeSrv","IngredientSrv","ProductSrv",
    "CookieSrv","GroceryListSrv",
    function($scope, $state,$ionicModal,UserSrv, RecipeSrv, IngredientSrv, ProductSrv, CookieSrv,GroceryListSrv) {
      $scope.recipes  =[];
      $scope.ingredients  = [];
      $scope.recipeProducts  = [];
      $scope.ingredientProducts  = [];
      $scope.choosenProducts  = [];


      function updateRecipes(){
        RecipeSrv.getChoosenRecipes().success(function(crs){
          console.log(crs);
          $scope.recipes=crs;
        });
        IngredientSrv.getIngredientsByChoosenRecipes().success(function(ings){
          console.log(ings);
          $scope.ingredients = ings;
        });
        ProductSrv.getProductsByChoosenRecipes().success(function(prods){
          console.log(prods);
          $scope.recipeProducts = prods;
        });
      }
      function updateProducts(){
        ProductSrv.getChoosenProducts().success(function(prods){
          $scope.choosenProducts=prods;
        });
      }

      $scope.showProductsByIngredient = function(id){
        ProductSrv.getProductsByIngredient(id).success(function(ings){
          console.log(ings);
          $scope.ingredientProducts = ings;
        });
      }

      $scope.rejectRecipe = function(id){
        RecipeSrv.rejectRecipe(id).success(function(){
          console.log("rejected");        
          updateRecipes();
        });
      }

      $scope.chooseProduct = function(id){
        console.log(id);
        ProductSrv.chooseProduct(id).success(function(){
          console.log("added");
          updateIngredientProducts(id, true);
          updateRecipeProducts(id,true);
          updateProducts();
        });

      }

      $scope.rejectProduct = function(id){
        ProductSrv.rejectProduct(id).success(function(){
          console.log("removed");
          updateIngredientProducts(id, false);
          updateRecipeProducts(id,false);
          updateProducts();
        });
      }

      function updateIngredientProducts(id, b){
        $.each($scope.ingredientProducts, function (i,obj) {
          if(obj.Id == id){
            obj.IsChoosen = b;
          }else if(id == null){
            obj.IsChoosen = b;
          }
          $scope.$apply();
        });
      }

      function updateRecipeProducts(id,b){
        $.each($scope.recipeProducts, function (i,obj) {
          if(obj.Id == id){
            obj.IsChoosen = b;
          }else if(id == null){
            obj.IsChoosen = b;
          }
          $scope.$apply();
        });
      }


      updateRecipes();
      updateProducts();

      $scope.name = "";
      $ionicModal.fromTemplateUrl('js/pages/save_list.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.modal = modal
      }) 

      $scope.saveModal = function(name) {
        console.log(name);
        CookieSrv.saveCookie(name).success(function(id){
          console.log(id);
          var note ={
            Id:id,
            Name:name,
            Recipes:$scope.recipes,
            Ingredients:$scope.ingredients,
            Products:$scope.products,
          }
          GroceryListSrv.addGroceryList(note);

          $state.go("saved_grocery_lists");
        })
        $scope.modal.hide();
      };

      $scope.closeModal = function(){ 
        $scope.modal.hide();
      }

      $scope.toSave = function (name) {
        $scope.modal.show();     
      }

      $scope.toCancel = function(){
        CookieSrv.cancelCookie().success(function(){
          $state.go("recipes");
        })
      }



    }]);


