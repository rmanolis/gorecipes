app.controller('SavedGroceryListsCtrl', 
    ["$scope","UserSrv","GroceryListSrv",
    function($scope,UserSrv,GroceryListSrv) { 
      function getGroceries(){
        $scope.groceries = [];        
        var objs = GroceryListSrv.getGroceryLists();
        console.log(objs);
        $scope.groceries = objs;
      }
      getGroceries();
      $scope.deleteGrocery = function(value){
        GroceryListSrv.deleteGroceryList(value.Name);
        getGroceries();  
      }

    }]);

