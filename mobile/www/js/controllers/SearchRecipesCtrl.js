app.controller('SearchRecipesCtrl', 
    ["$scope","$q", "$rootScope", "IngredientSrv", "TagSrv","RecipeSrv","UserSrv",
    function($scope,$q, $rootScope,IngredientSrv, TagSrv,RecipeSrv,UserSrv) { 
      $scope.sizeList = 5;
      $scope.totalPages = 0;
      $scope.name = "";
      $scope.stags =[];
      $scope.singredients = [];
      $scope.recipes  = [];
      $scope.page = 1;
      $scope.clickedValueModel = "";
      $scope.removedValueModel = "";
      function asyncForAutocomplete(cb,query){
        var d = $q.defer();
        cb(query).success(function(tags){
          var items = [];
          angular.forEach(tags,function(tag){
            items.push({id:tag.Id, name:tag.Name, view:tag.Name})
          })
          d.resolve({items:items});
        }).error(function(){
          d.reject({items:[]});
        })
        return d.promise

      }
    
      function searchRecipes(){
        console.log($scope.stags);
        RecipeSrv.searchRecipes($scope.name,$scope.stags,$scope.singredients,
            $scope.page,$scope.sizeList).success(function(lo){
          console.log(lo);
          $scope.totalPages = Math.round(lo.Total /lo.Size);
          console.log($scope.totalPages);
          $scope.recipes = lo.Objects;
        });
      }

      $scope.changePage = function(page){
        $scope.page = page;
        $scope.submit();
      }

      $scope.getTags = function (query) {
        return asyncForAutocomplete(TagSrv.getTags,query);
      };
      $scope.getIngredients = function (query) {
        return asyncForAutocomplete(IngredientSrv.getIngredients,query)
      };



      $scope.submit = function() {
        searchRecipes();
      }

      $scope.submit();


      $scope.choose=function(recipe){
        console.log(recipe.Id);
        RecipeSrv.chooseRecipe(recipe.Id).success(function(){
          console.log("added");
          recipe.IsChoosen = true;
        })
      }

      $scope.reject=function(recipe){
        console.log(recipe.Id);
        RecipeSrv.rejectRecipe(recipe.Id).success(function(){
          recipe.IsChoosen = false;
          
        })
      }


    }]);


