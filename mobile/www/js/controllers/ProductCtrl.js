app.controller('ProductCtrl', 
    ["$scope","$state","ProductSrv",
    function($scope, $state, ProductSrv) {
     $scope.product = {};
      ProductSrv.getProduct($stateParams.id).success(function(product){
        $scope.product = product;
      });

    $scope.choose = function(){
     ProductSrv.chooseProduct($scope.product.Id).success(function(){
      $scope.product.IsChoosen = true; 
     })
    }

    $scope.reject = function(){
       ProductSrv.rejectProduct($scope.product.Id).success(function(){
        $scope.product.IsChoosen = false; 
       })
    }

 

    }]);
