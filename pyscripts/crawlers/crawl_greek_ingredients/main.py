import bs4
import requests
import json
import io
ls_words = []
for i in range(23):
    req = requests.get("http://www.thecookbook.gr/summary.asp?catid="+\
               "28457&forder1=ART_TITLE&forder1vl=" + str(i) +"#letter")
    bs = bs4.BeautifulSoup(req.text,'html.parser')
    words = bs.findAll("strong")
    for w in words:
        ls_words.append(w.get_text().lower())


with io.open('ingredients.json', 'w', encoding="utf8") as outfile:
    data = json.dumps(ls_words, ensure_ascii=False,
                      sort_keys=True, indent=4)
    outfile.write(unicode(data))
