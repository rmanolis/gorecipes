
<!-- .page-banner -->
<div class="page-wrapper single-recipe">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-main">
				<main id="main" class="main">
					<article class="hentry">
						<div class="row">
							<div class="col-md-7">
								<div class="recipe-slider-wrapper">
									<div class="single-recipe-carousel">
                      {% if .hasPhoto %}
									  <img  style="width:668px;height:535px;" class="img-responsive" 
                        ng-src="/static/photos/r_{% .id %}.jpg" alt="Recipe"> 
                        {% end %}

									</div>
									<div class="single-recipe-carousel-nav">
									</div>
									<div class="links">
                        <div id="share-icons">

                        </div>
                        
                        <a class="page-print" href="javascript:window.print()"><i class="fa fa-print"></i>Print</a>
                        <span class="published-date">
                            <i class="fa fa-clock-o"></i><time class="entry-date published" datetime="{% .date %}">{% .dateStr %}</time>
                        </span>
									</div>
								</div>
								<section class="widget category-widget">
									<h4 class="heading-bottom-line">ΥΛΙΚΑ</h4>
                  <ul>
                  {% range $key, $value := .ingredients %}
                        <li >
                         <i class="fa fa-angle-right"></i><span>{% $value %}</span>
                        </li>
                  {% end %}
                  </ul>
                </section>
								<section class="widget category-widget" >
									  <h4 class="heading-bottom-line">TAGS</h4>                    
                  	<p> 
                    {% range $key, $value := .tags %} 
                               {% $value %},
                    {%end%}
                    </p>
                </section>

                </div>
							<div class="col-md-5">
								<div class="recipe-top-meta clearfix" >
									<span class="entry-author author vcard"><span class="screen-reader-text">By:</span>
                        <strong>{% .userName %}</strong></span> 
                </div>
                <div class="row" ng-controller="RecipeCtrl">
                  <div class="col-sm-10">
								    <h2>{%.name%}</h2> 
                  </div>
                  <div class="col-sm-2">
                    <div ng-if="!recipe.IsChoosen">
                      <button class="btn btn-info" ng-click="choose(recipe.Id)"> 
                      <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>&nbsp;Save</button>
                    </div>
                    <div ng-if="recipe.IsChoosen">
                      <button class="btn btn-warning" ng-click="reject(recipe.Id)">
                      <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>&nbsp;Reject</button>
                    </div>
                  </div>
                </div>
                <section class="widget category-widget">
                  <h3 class="heading-bottom-line">ΤΙ ΧΡΕΙΑΖΟΜΑΣΤΕ</h3>
								  <textarea style="height:400px;" readonly disabled>{% .servings %}</textarea>
                </section>
                <section class="widget category-widget">
                  <h3 class="heading-bottom-line">ΠΩΣ ΤΟ ΚΑΝΟΥΜΕ</h3>
                  <textarea style="height:400px;" readonly disabled>{% .execution  %}</textarea>
                </section>
							</div>
						</div>
					</article>
				</main>
				<!-- .main -->
			</div>
			<!-- .col-main -->
		</div>
		<!-- .row -->
	</div>
	<!-- .container -->
</div>




