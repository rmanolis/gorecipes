<div ng-controller="GroceryListsCtrl">
<!-- .page-banner -->
<div id="page-layer-styled" class="page-wrapper">
	<div id="left-layer"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-main">
         <section class="widget category-widget">
            <h2 class="heading-bottom-line">Grocery Lists</h2>
            <div ng-repeat="list in grocery_lists">
              <div class="row">
               <div class="col-md-6">
                <i class="fa fa-angle-right"></i>
                <strong> {{list.doc.Name}} </strong>
                </div>
                <div class="col-md-6">
                <button class="btn btn-info" ng-click="showList(list)">show</button>
                <button class="btn btn-danger" ng-click="deleteList(list)">delete</button>
                </div>
              </div>
            </div>
          </section>
				<!-- .main -->
			</div>
      <div ng-show="groceries" class="col-md-8 col-main">
        <section class="widget category-widget">
          <h3 class="heading-bottom-line">{{groceries.Name}}</h3>
          <div class="row">
            <div class="col-md-3">
            <h2>Ingredients</h2> 
              <ul ng-repeat="ing in groceries.Ingredients">
                <li> {{ing}} </li>
              </ul>
             </div>
             <div class="col-md-9">
              <h2>Recipes</h2>
                <ul ng-repeat="recipe in groceries.Recipes">
                  <li>
                     	<article class="recipe recipe-listing hentry clearfix">
                        <figure class="col-md-3 recipe-thumbnail thumbnail-hover">
                          <div ng-if="recipe.HasPhoto">
                            <img  style="width:300px;height:200px;" class="img-responsive" ng-src="/static/photos/r_{{recipe.Id}}.jpg" alt="Recipe"> 
                          </div>
                        </figure>
                        <div class="col-md-9 recipe-column">
                          <div class="recipe-top-meta clearfix">
                            <span class="entry-author author vcard"><span class="screen-reader-text">
                            By:</span><strong >{{recipe.UserName}}</strong></span>
                          </div>
                          <div class="recipe-details">
                            <h2 class="entry-title recipe-title"><a href="{{'/recipes/'+recipe.Id}}">{{recipe.Name}}</a></h2>
                            <p>{{recipe.SIngredients | limitTo: 30}}{{recipe.SIngredients > 30 ? '...' : ''}}</p>
                            <footer class="entry-meta recipe-entry-meta recipe-bottom-meta clearfix">
                                  <span class="published-date">
                                  <i class="fa fa-clock-o"></i><time class="entry-date published" datetime="{{recipe.Date}}">
                              {{recipe.Date  | date:'MMMM d, yyyy'}}</time>
                                  </span> <a href="{{'/recipes/'+recipe.Id}}" class="read-more">Read more</a>
                            </footer>
                          </div>
                        </div>
                      </article>

                  </li>
                </ul>
              </div>
          </div>
        </section>
      </div>
			<!-- .col-main -->
		</div>
		<!-- .row -->
	</div>
	<!-- .container -->
</div>


</div>
