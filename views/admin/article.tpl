
<div ng-controller="AdminArticleCtrl">
<div class="ui-widget">
Title: <input ng-model="article.Title">
<br/>

Link: <input ng-model="article.Link">
<br/>

Text: <ng-quill-editor ng-model="articleText" toolbar="true" link-tooltip="true" image-tooltip="true" toolbar-entries="font size bold list bullet italic underline strike align color background link image" editor-required="true" required="" error-class="input-error"></ng-quill-editor>
<br/>

<input custom-on-change="fileUpload" type="file" accept="image/jpeg" class="fileChooser"/>
<br/>
<img ng-if="article.HasPhoto" height="420" width="420" ng-src="/static/images/a_{{article.Id}}.jpg" />
<br/>
<button ng-if="article.HasPhoto" ng-click="deletePicture()">Delete</button>
<br/>
<button ng-click="submit()">Submit</button>
<button ng-click="cancel()">Cancel</button>

</div>
</div>

