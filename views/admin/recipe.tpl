
<div ng-controller="AdminRecipeCtrl">
<div class="ui-widget">
Name: <input ng-model="name">
<br/>
Tags: <input id="tags" size="50" ng-model="stags"  >
<br/>
Ingredients: <input id="ingredients" ng-model="singredients" size="50" >
<br/>
Products: <input id="products" size="50"  ng-models="sproducts">
<br/>
<input custom-on-change="fileUpload" type="file" accept="image/jpeg" class="fileChooser"/>

<br/>
Servings: 
<textarea  ng-model="servings" id="recipe-short-description" name="recipe-short-description" data-msg-required="Please enter some short description" data-rule-required="true" aria-required="true"></textarea> 
<br/>

Execution: 
<textarea  ng-model="steps" id="recipe-short-description" name="recipe-short-description" data-msg-required="Please enter some short description" data-rule-required="true" aria-required="true"></textarea> 

<br/>

<img ng-if="has_photo" height="420" width="420" ng-src="/static/images/r_{{id}}.jpg" />
<br/>
<button ng-if="has_photo" ng-click="deletePicture()">Delete</button>
<br/>


<button ng-click="submit()">Submit</button>
<button ng-click="cancel()">Cancel</button>

</div>
</div>

