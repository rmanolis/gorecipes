<div ng-controller="SearchRecipesCtrl">
<!-- .page-banner -->
<div id="page-layer-styled" class="page-wrapper">
	<div id="left-layer"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-main">
				<main id="main" class="main">
					<div class="row recipe-wrapper" ng-repeat="recipe in recipes">
						<div class="col-xs-6 col-md-12 recipe-column">
							<article class="recipe recipe-listing hentry clearfix">
								<figure class="col-md-6 recipe-thumbnail thumbnail-hover">
                  <div ng-if="recipe.HasPhoto">
									  <img  style="width:370px;height:260px;" class="img-responsive" ng-src="/static/photos/r_{{recipe.Id}}.jpg" alt="Recipe"> 
                  </div>
                </figure>
								<div class="col-md-6 recipe-column">
                  <div class="row">
                    <div class="col-md-9 recipe-top-meta clearfix">
                      <span class="entry-author author vcard"><span class="screen-reader-text">
                      By:</span><strong >{{recipe.UserName}}</strong></span>
                    </div>
                    <div class="col-md-1" ng-if="!recipe.IsChoosen">
                        <button class="btn btn-info" ng-click="choose(recipe.Id)"> 
                        <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></button>
                      </div>
                      <div class="col-md-1" ng-if="recipe.IsChoosen">
                        <button class="btn btn-warning" ng-click="reject(recipe.Id)">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    </div>
                  </div>
									<div class="recipe-details">
										<h2 class="entry-title recipe-title"><a href="{{'/recipes/'+recipe.Id}}">{{recipe.Name}}</a></h2>
										<p>{{recipe.SIngredients | limitTo: 30}}{{recipe.SIngredients > 30 ? '...' : ''}}</p>
										<footer class="entry-meta recipe-entry-meta recipe-bottom-meta clearfix">
                          <span class="published-date">
                          <i class="fa fa-clock-o"></i><time class="entry-date published" datetime="{{recipe.Date}}">
                      {{recipe.Date  | date:'MMMM d, yyyy'}}</time>
                          </span> <a href="{{'/recipes/'+recipe.Id}}" class="read-more">Read more</a>
										</footer>
									</div>
								</div>
							</article>
						</div>
					</div>
        <div id="pagination"></div>          
				</main>
				<!-- .main -->
			</div>
			<!-- .col-main -->
			<div class="col-md-4 col-sidebar">
				<aside id="sidebar" class="sidebar">
					<section class="widget">
						<h3 class="widget-title heading-bottom-line">Search</h3>
            <div class="row">
              <div class="col-sm-8">
                <div class="form-group">
                   <input type="text" placeholder="ψάξε με βάση το όνομα" class="form-control" value="" ng-model="name">
                </div>
              </div>
              <div class="col-sm-2">
                 <div >
                    <input type="submit" id="searchsubmit"  ng-click="submit()" value="Search" >
                 </div>
              </div>
            </div>
             <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                   <input type="text"  placeholder="ψάξε με βάση τα tags: χορτοφαγια, κινεζικο"  id="tags" size="150" ng-model="stags">
                </div>
              </div>
             </div>
             <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                   <input type="text" placeholder="ψάξε με βάση τα υλικά: πατατα, -σκορδο"  id="ingredients" size="150" ng-model="singredients">
                </div>
              </div>          
            </div>
					</section>
          <div ng-controller="BasketCtrl">
            <section  class="widget category-widget">		
              <h3 class="widget-title heading-bottom-line">ΕΠΙΛΕΓΜΕΝΕΣ ΣΥΝΤΑΓΕΣ</h3>
              <ul ng-repeat="recipe in choosenRecipes">
                <li class="row">
                   <div class="col-md-8">
                   <i class="fa fa-angle-right"></i> <a href="{{'/recipes/'+ recipe.Id }}" > <strong >{{recipe.Name}}</strong></a>
                   </div>
                    <div class="col-md-2">
                      <button class="btn btn-warning" ng-click="rejectRecipe(recipe.Id)">
                      <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>Reject</button>
                    </div>
                </li>
              </ul>
              <br/>
              <div ng-if="choosenRecipes.length > 0" class="row">
                <button class="col-md-5 btn btn-success" ng-click="save()">Save</button>
                <span class="col-md-2">&nbsp;</span>
                <button class="col-md-5 btn btn-warning" ng-click="cancel()">Cancel</button>
              </div>
            </section>
            <section class="widget category-widget">
              <h3 class="widget-title heading-bottom-line">ΥΛΙΚΑ</h3>
              <ul ng-repeat="ingredient in ingredients">
                <li><i class="fa fa-angle-right"></i>{{ingredient.Name}}</li>
              </ul>
            </section>
          </div>
				</aside>
				<!-- .sidebar -->
			</div>
			<!-- .col-sidebar -->
		</div>
		<!-- .row -->
	</div>
	<!-- .container -->
</div>

</div>

<script type="text/ng-template" id="grocerySaveContent.html">
        <div class="modal-header">
            <h3 class="modal-title">Save</h3>
        </div>
        <div class="modal-body">
            <input ng-model="name" type="text"/>
        </b>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="button" ng-click="ok()">OK</button>
            <button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
        </div>
</script>

