<div ng-controller="UserRecipesCtrl">
<!-- .page-banner -->
<div id="page-layer-styled" class="page-wrapper">
	<div id="left-layer"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-main">
				<main id="main" class="main">
					<div class="row recipe-wrapper" ng-repeat="recipe in recipes">
						<div class="col-xs-6 col-md-12 recipe-column">
							<article class="recipe recipe-listing hentry clearfix">
								<figure class="col-md-6 recipe-thumbnail thumbnail-hover">
                  <div ng-if="recipe.HasPhoto">
									  <img  style="width:370px;height:260px;" class="img-responsive" ng-src="/static/photos/r_{{recipe.Id}}.jpg" alt="Recipe"> 
                  </div>
								</figure>
								<div class="col-md-6 recipe-column">
									<div class="recipe-top-meta clearfix">
                    <div class="row">
                      <div class="col-md-9 recipe-top-meta clearfix">
                        <span class="entry-author author vcard"><span class="screen-reader-text">By:</span>
                        <strong >{{recipe.UserName}}</strong>
                        </span>
                      </div>
                      <div class="col-md-1">
                        <a  class="btn btn-success" href="/user/recipes/{{recipe.Id}}">Edit</a>
                      </div>
                    </div>
									</div>
									<div class="recipe-details">
										<h2 class="entry-title recipe-title"><a href="{{'/recipes/'+recipe.Id}}">{{recipe.Name}}</a></h2>
										<p>{{recipe.SIngredients | limitTo: 30}}{{recipe.SIngredients > 30 ? '...' : ''}}</p>
										<footer class="entry-meta recipe-entry-meta recipe-bottom-meta clearfix">
                          <span class="published-date">
                          <i class="fa fa-clock-o"></i><time class="entry-date published" datetime="{{recipe.Date}}">
                      {{recipe.Date  | date:'MMMM d, yyyy'}}</time>
                          </span> <a href="{{'/recipes/'+recipe.Id}}" class="read-more">Read more</a>
										</footer>
									</div>
								</div>
							</article>
						</div>
					</div>
        <div id="pagination"></div>          
				</main>
				<!-- .main -->
			</div>
			<!-- .col-main -->
			<div class="col-md-4 col-sidebar">
				<aside id="sidebar" class="sidebar">
					<section class="widget">
						<h3 class="widget-title heading-bottom-line">Search</h3>
            <div class="row">
              <div class="col-sm-8">
                <div class="form-group">
                   <input type="text" placeholder="ψάξε με βάση το όνομα" class="form-control" value="" ng-model="name">
                </div>
              </div>
              <div class="col-sm-2">
                 <div >
                    <input type="submit" id="searchsubmit"  ng-click="submit()" value="Search" >
                 </div>
              </div>
            </div>
					</section>
        </aside>
				<!-- .sidebar -->
			</div>
			<!-- .col-sidebar -->
		</div>
		<!-- .row -->
	</div>
	<!-- .container -->
</div>

</div>


