

<!-- .page-banner -->
<div ng-controller="UserSubmitRecipeCtrl" id="page-layer-styled" class="page-wrapper submit-recipe">
	<div id="left-layer"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-main">
				<main id="main" class="main">
					<article class="hentry">
						<h3 class="entry-title heading-bottom-line">Welcome to Recipe <span>contributor</span></h3>
						<div class="entry-content">
						
						</div>
					</article>
					<div id="recipe-submit-form" name="recipe-submit-form" >
						<p>
							<label for="recipe-title">Recipe Title</label> <input type="text" id="recipe-title" value="" name="recipe-title" data-msg-required="Please enter recipe title" data-rule-required="true" aria-required="true"  ng-model="name"> <span class="note">Keep it short and descriptive</span>
						</p>

            <p >
                <label>Tags</label> <input type="text" id="tags" size="50" ng-model="stags"  >
            </p>
            <p >
                <label>Ingredients</label> <input type="text" id="ingredients" size="50" ng-model="singredients"  >
            </p>

						<p>
							<label for="recipe-picture">Upload picture</label> <input custom-on-change="fileUpload" type="file" accept="image/jpeg" class="fileChooser"/>
						</p>
            <p>
							<label for="recipe-short-description">ΤΙ ΧΡΕΙΑΖΟΜΑΣΤΕ</label>  <textarea  ng-model="servings" id="recipe-short-description" name="recipe-short-description" data-msg-required="Please enter some short description" data-rule-required="true" aria-required="true"></textarea> 
            </p>
            <p>
              <label for="recipe-additional-notes">ΠΩΣ ΤΟ ΚΑΝΟΥΜΕ</label>  <textarea  ng-model="steps" id="recipe-short-description" name="recipe-short-description" data-msg-required="Please enter some short description" data-rule-required="true" aria-required="true"></textarea>            
            </p>

				</div>                       
						<p>
							<label>Note:</label> <span class="note">The submitted recipe will only be visible to you until it has been approved by an adminstrator.</span>
						</p>
						<input type="submit" value="Submit Recipe" name="submit" ng-click="submit()">
					</div>
				</main>
				<!-- .main -->
			</div>
			</aside>
				<!-- .sidebar -->
			</div>
			<!-- .col-sidebar -->
		</div>
		<!-- .row -->
	</div>
	<!-- .container -->
</div>

