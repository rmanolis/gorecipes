<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#">
<head>
<title>Edesmata</title>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta property="og:type" content="website" />
<meta property="og:title" content="Εδέσματα" />
<meta property="og:url" content="http://www.edesmata.com/" />
<meta property="og:description" content="Οργάνωσε τις τέλειες συνταγές"/>
<meta property="og:image" content="http://www.edesmata.com/static/images/site/logo.png" />
<meta name="author" content="Manos Ragiadakos"/>

<!-- Google fonts -->
<link href='http://fonts.googleapis.com/css?family=Raleway:400,900,800,700,600,500,300,200,100' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800italic,800,700,600italic,300italic,300,700italic,600,400italic' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="/static/bower_components/animate.css/animate.min.css">
<link rel="stylesheet" href="/static/bower_components/components-font-awesome/css/font-awesome.min.css">


<link rel="stylesheet" href="/static/bower_components/jquery-ui/themes/smoothness/jquery-ui.min.css">
<link rel="stylesheet" href="/static/bower_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="/static/bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/static/css/recipex.css">

<script type="text/javascript" src="/static/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="/static/bower_components/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/static/bower_components/lodash/lodash.min.js"></script>    
<script type="text/javascript" src="/static/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/static/bower_components/angular/angular.min.js"></script>
<script type="text/javascript" src="/static/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
<script type="text/javascript" src="/static/bower_components/moment/min/moment.min.js"></script>
<script type="text/javascript" src="/static/bower_components/bootpag/lib/jquery.bootpag.min.js"></script>
<script type="text/javascript" src="/static/bower_components/html2canvas/build/html2canvas.min.js"></script>
<script type="text/javascript" src="/static/bower_components/ng-file-upload/ng-file-upload-all.min.js"></script>
<script type="text/javascript" src="/static/bower_components/pouchdb/dist/pouchdb.min.js"></script>

<script  type="text/javascript" src="/static/js/app/main.js"></script> 


<script  type="text/javascript" src="/static/js/app/services/StorageSrv.js"></script> 
<script  type="text/javascript" src="/static/js/app/services/GroceryListSrv.js"></script> 
<script  type="text/javascript" src="/static/js/app/services/CookieSrv.js"></script> 
<script  type="text/javascript" src="/static/js/app/services/HelperSrv.js"></script> 
<script  type="text/javascript" src="/static/js/app/services/IngredientSrv.js"></script> 
<script  type="text/javascript" src="/static/js/app/services/ProductSrv.js"></script> 
<script  type="text/javascript" src="/static/js/app/services/RecipeSrv.js"></script> 
<script  type="text/javascript" src="/static/js/app/services/TagSrv.js"></script> 
<script  type="text/javascript" src="/static/js/app/services/UserSrv.js"></script> 
<script  type="text/javascript" src="/static/js/app/services/ArticleSrv.js"></script> 
<script  type="text/javascript" src="/static/js/app/services/AutocompleteSrv.js"></script> 

<script  type="text/javascript" src="/static/js/app/directives/CustomOnChangeDrv.js"></script> 

<script  type="text/javascript" src="/static/js/app/controllers/BasketCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/RecipeCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/SearchRecipesCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/GroceryListsCtrl.js"></script> 

<script  type="text/javascript" src="/static/js/app/controllers/user/UserSettingsCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/user/UserRecipesCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/user/UserSubmitRecipeCtrl.js"></script> 

<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminArticleCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminListArticlesCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminListIngredientsCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminIngredientCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminListRecipesCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminRecipeCtrl.js"></script>
<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminListProductsCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminProductCtrl.js"></script>
<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminListTagsCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminTagCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminListCookiesCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminListUsersCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminUserCtrl.js"></script> 
<script  type="text/javascript" src="/static/js/app/controllers/admin/AdminStatisticsCtrl.js"></script>


</head>
<body class="home" ng-app="app">
<header class="master-header header-two">
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-lg-6">
                 {% if .IsAdmin %}
                 <ul class="header-links-list">
                        <li><a href="/admin/tags">Tags</a></li>    
                        <li><a href="/admin/ingredients">Ingredients</a></li> 
                        <li><a href="/admin/recipes">Recipes</a></li> 
                        <li><a href="/admin/products">Products</a></li> 
                        <li><a href="/admin/users">Users</a></li> 
                        <li><a href="/admin/statistics">Statistics</a></li>
                        
                  </ul>
                  {% else %}
                    <div class="social-media">
                    </div>
                    {% end %}
                </div>
                <div class="col-md-7 col-lg-6">
                    <ul class="header-links-list">
                        {% if .IsUser %}
                        <li><a href="/user/settings">Settings</a></li>
                        <li><a href="/user/recipes">My Recipes</a></li>
                        <li><a href="/user/recipes/submit">Submit Recipe</a></li>
                        <li><a href="/auth/logout">Logout</a></li>
                        {% else %}
                        <li><a href="/auth/login">Login</a></li>
                        <li><a href="/auth/register">Register</a></li>
                        {% end %}
                       
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom">
        <div class="container">
            <div class="logo-wrapper">
                <a class="site-logo" href="/"><img src="/static/images/site/logo.png" alt="Logo"/></a>
            </div>
            <div class="mobile-menu hidden-md hidden-lg"></div>
            <nav class="primary-nav">
                <ul class="primary-menu clearfix">
                    <li class="home-menu-item">
                        <a href="/">Home</a>
                    </li>
                    <li class="blog-menu-item">
                        <a href="/grocery_lists">Grocery Lists</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>


<div>
{% yield %}
</div>

<!-- .page-wrapper -->
<footer class="master-footer footer-two">
    <div class="footer-top">
        <div id="footer-left-layer"></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-3 footer-widget">
                </div>
                <div class="col-sm-6 col-md-3 footer-widget">
                </div>
                <div class="clearfix visible-sm"></div>
                <div class="col-sm-6 col-md-3 footer-widget">
                </div>
                <div class="col-sm-6 col-md-3 footer-widget">
                    <section class="widget widget-contact-details">
                        <h3 class="widget-title">Get in Touch</h3>
                        <ul>
                            <li>
                                <i class="fa fa-envelope-o"></i>
                                <div class="wrapper">
                                    <a href="mailto:manos.ragiadakos@gmail.com">manos.ragiadakos@gmail.com</a>
                                </div>
                            </li>
                            <li>
                                <i class="fa fa-globe"></i>
                                <div class="wrapper">
                                    <a href="http://www.edesmata.com">http://www.edesmata.com</a>
                                </div>
                            </li>
                        </ul>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <p class="footer-bottom-text">
                Copyright © Edesmata 2016. All rights reserved. Created by Manos Ragiadakos
            </p>
        </div>
    </div>
</footer>

</body>

</html>
