package viewers

import (
	"gorecipes/confs"
	"gorecipes/models"
	"gorecipes/utils"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

func ShowRecipes(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	size, page := utils.SizePage(r)
	lo, err := models.GetRecipes(db, size, page)
	if err != nil {
		confs.LOG.Error.Println(err.Error())
	}
	data := map[string]interface{}{
		"paginator": utils.NewPaginator(r, size, lo.Total),
		"recipes":   lo.Objects.([]models.Recipe),
	}
	utils.Render(w, r, "visitor/recipes", data)
}

func ShowRecipe(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	vars := mux.Vars(r)
	id := vars["id"]
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	bid := bson.ObjectIdHex(id)
	rec := new(models.Recipe)
	err := models.FindRecipeId(db, bid, rec)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	stags := []string{}
	for _, v := range rec.Tags {
		tag := new(models.Tag)
		models.FindTagId(db, v, tag)
		stags = append(stags, tag.Name)
	}

	sings := []string{}
	for _, v := range rec.Ingredients {
		ing := new(models.Ingredient)
		models.FindIngredientId(db, v, ing)
		sings = append(sings, ing.Name)
	}

	if rec.UserId.Valid() {
		user, err := models.GetUserById(db, rec.UserId)
		if err == nil {
			rec.UserName = user.Name
		}
	}

	data := map[string]interface{}{}
	data["id"] = rec.Id.Hex()
	data["hasPhoto"] = rec.HasPhoto
	data["name"] = rec.Name
	data["servings"] = rec.Servings
	data["execution"] = rec.Steps
	data["tags"] = stags
	data["ingredients"] = sings
	data["date"] = rec.Date
	data["dateStr"] = rec.Date.Format("January 02, 2006")
	data["userName"] = rec.UserName

	utils.Render(w, r, "visitor/recipe", data)
}

func ShowSearchRecipes(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, r, "visitor/search_recipes", nil)
}

func ShowChoosenRecipes(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, r, "visitor/groceries", nil)
}
