package viewers

import (
	"gorecipes/utils"
	"net/http"
)

func ShowUserSettings(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, r, "user/settings", nil)
}

func ShowMyRecipes(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, r, "user/my_recipes", nil)
}

func ShowSubmitRecipe(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, r, "user/submit_recipe", nil)
}
