package viewers

import (
	"gorecipes/confs"
	"gorecipes/models"
	"gorecipes/utils"
	"net/http"
)

func ShowLogin(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, r, "auth/login", nil)
}

func PostLogin(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	r.ParseForm()
	user := new(models.User)
	user.Email = r.FormValue("Email")
	user.Password = r.FormValue("Password")
	err := user.Authenticate(db)
	if err != nil {
		confs.LOG.Info.Println("have not been found")
		http.Redirect(w, r, "/auth/login", 302)
		return
	}
	utils.SetSession(w, r, "user_id", user.Id.Hex())
	if user.IsAdmin {
		utils.SetSession(w, r, "admin_id", user.Id.Hex())
	}
	confs.LOG.Info.Println("Have been found with Name " + user.Name)
	http.Redirect(w, r, "/", 302)
}

func ShowLogout(w http.ResponseWriter, r *http.Request) {
	utils.DeleteCookies(w, r)
	http.Redirect(w, r, "/", 302)
}

func ShowRegister(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, r, "auth/register", nil)
}

func PostRegister(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	r.ParseForm()

	user := new(models.User)
	user.Name = r.FormValue("Name")
	user.Password = r.FormValue("Password")
	user.Email = r.FormValue("Email")
	user.IsAdmin = true
	err := user.Insert(db)
	if err != nil {
		http.Redirect(w, r, "/auth/register", 302)
		return
	}
	http.Redirect(w, r, "/auth/login", 302)
}
