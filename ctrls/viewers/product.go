package viewers

import (
	"gorecipes/confs"
	"gorecipes/models"
	"gorecipes/utils"
	"html/template"

	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

func ShowProducts(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	size, page := utils.SizePage(r)
	lo, err := models.GetProducts(db, size, page)
	if err != nil {
		confs.LOG.Error.Println(err.Error())
	}
	data := map[string]interface{}{
		"paginator": utils.NewPaginator(r, size, lo.Total),
		"products":  lo.Objects.([]models.Product),
	}
	utils.Render(w, r, "visitor/products", data)
}

func ShowProduct(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	vars := mux.Vars(r)
	id := vars["id"]
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	bid := bson.ObjectIdHex(id)
	prod := new(models.Product)
	err := models.FindProductId(db, bid, prod)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	stags := ""
	for _, v := range prod.Tags {
		tag := new(models.Tag)
		models.FindTagId(db, v, tag)
		stags += tag.Name + ", "
	}

	sings := ""
	for _, v := range prod.Ingredients {
		ing := new(models.Ingredient)
		models.FindIngredientId(db, v, ing)
		sings += ing.Name + ", "
	}
	data := map[string]interface{}{}
	data["name"] = prod.Name
	data["description"] = template.HTML(prod.Description)
	data["price"] = prod.Price
	data["tags"] = stags
	data["ingredients"] = sings

	confs.LOG.Error.Println(prod)
	utils.Render(w, r, "visitor/product", data)

}

func ShowSearchProducts(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, r, "visitor/search_products", nil)
}
