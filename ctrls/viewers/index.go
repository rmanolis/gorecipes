package viewers

import (
	"gorecipes/utils"
	"net/http"
)

func ShowIndex(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, r, "index", nil)
}
