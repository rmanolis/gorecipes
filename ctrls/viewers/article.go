package viewers

import (
	"gorecipes/models"
	"gorecipes/utils"
	"html/template"
	"net/http"

	"github.com/gorilla/mux"
)

func ShowArticle(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	vars := mux.Vars(r)
	link := vars["link"]
	article := new(models.Article)
	err := models.FindArticleByLink(db, link, article)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	data := map[string]interface{}{}

	data["Title"] = article.Title
	data["Text"] = template.HTML(article.Text)
	utils.Render(w, r, "visitor/article", data)
}
