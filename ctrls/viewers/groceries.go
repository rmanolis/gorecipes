package viewers

import (
	"gorecipes/utils"
	"net/http"
)

func ShowGroceryLists(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, r, "visitor/grocery_lists", nil)
}
