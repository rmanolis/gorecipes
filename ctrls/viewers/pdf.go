package viewers

import (
	"gorecipes/models"
	"gorecipes/utils"
	"net/http"
)

func ShowPDF(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	cookie, err := utils.GetCookieId(r)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	cobj.IsPDFed = true
	cobj.Update(db)
	data := map[string]interface{}{}
	data["products"] = cobj.GetProducts(db)
	data["recipes"] = cobj.GetRecipes(db)
	data["ingredients"], _ = cobj.GetIngredientsByRecipes(db)
	utils.NoLayoutRender(w, r, "visitor/pdf", data)
}
