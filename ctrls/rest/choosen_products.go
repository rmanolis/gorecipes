package rest

import (
	"encoding/json"
	"gorecipes/confs"
	"gorecipes/models"
	"gorecipes/utils"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

func GetChoosenProducts(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	cookie, err := utils.GetCookieId(r)
	if err != nil {
		confs.LOG.Error.Println(err.Error())
		http.Error(w, "no cookie", http.StatusInternalServerError)
		return
	}

	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		confs.LOG.Error.Println(err.Error())
		http.Error(w, "no cookie", http.StatusInternalServerError)
		return
	}

	prods := cobj.GetProducts(db)
	b, err := json.Marshal(prods)
	if err != nil {
		confs.LOG.Error.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(b)
}

func ChooseProduct(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	vars := mux.Vars(r)
	id := vars["id"]
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	product := new(models.Product)
	err := models.FindProductId(db, bson.ObjectIdHex(id), product)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	cookie, err := utils.GetCookieId(r)
	if err != nil {
		http.Error(w, "no cookie", http.StatusInternalServerError)
		return
	}

	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	cr := new(models.ChoosenProduct)
	cr.CookieId = cobj.Id
	cr.ProductId = product.Id
	err = cr.Insert(db)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func RejectProduct(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	vars := mux.Vars(r)
	id := vars["id"]
	cookie, err := utils.GetCookieId(r)
	if err != nil {
		http.Error(w, "no cookie", http.StatusInternalServerError)
		return
	}

	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	cr := new(models.ChoosenProduct)
	err = cobj.FindChoosenProduct(db, bson.ObjectIdHex(id), cr)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	err = cr.Remove(db)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

}
