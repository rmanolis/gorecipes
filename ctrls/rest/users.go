package rest

import (
	"encoding/json"
	"gorecipes/confs"
	"gorecipes/models"
	"gorecipes/utils"
	"net/http"

	"github.com/gorilla/context"
	"gopkg.in/mgo.v2/bson"
)

func UserEditPassword(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	r.ParseForm()
	password := r.FormValue("Password")
	rpassword := r.FormValue("RepeatPassword")
	user := context.Get(r, "user").(models.User)
	if password != rpassword {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}

	user.SetPassword(db, password)
	err := user.Update(db)
	if err != nil {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
}

func AuthUserHandler(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		db := utils.GetDB(r)
		user_id, err := utils.GetUserId(r)
		if err != nil {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		id := bson.ObjectIdHex(user_id)
		user, err := models.GetUserById(db, id)
		if err != nil {
			confs.LOG.Error.Println(err.Error())
			http.Error(w, err.Error(), http.StatusUnauthorized)
			return
		}

		context.Set(r, "user", user)
		next(w, r)
		context.Clear(r)
	}
}

func UserAddPicture(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	user := context.Get(r, "user").(*models.User)
	utils.SaveFile(r, "static/photos/u_"+user.Id.Hex()+".jpg")
	user.HasPhoto = true
	user.Update(db)
}

func UserDeletePicture(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	user := context.Get(r, "user").(*models.User)
	if user.HasPhoto {
		utils.DeleteFile("static/photos/u_" + user.Id.Hex() + ".jpg")
		user.HasPhoto = false
		user.Update(db)
	}
}

func UserInfo(w http.ResponseWriter, r *http.Request) {
	user := context.Get(r, "user").(*models.User)
	user.Password = ""
	b, _ := json.Marshal(user)
	w.Write(b)
}
