package rest

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
	"gorecipes/confs"
	"gorecipes/models"
	"gorecipes/utils"
	"net/http"
	"strings"
)

func SearchTags(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	data := struct {
		Name string
	}{}

	d := json.NewDecoder(r.Body)
	err := d.Decode(&data)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	size, page := utils.SizePage(r)
	lo, _ := models.SearchTags(db, data.Name, size, page)
	b, err := json.Marshal(lo)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	w.Write(b)
}

func ListTags(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	size, page := utils.SizePage(r)
	name := r.FormValue("Name")
	is_minus := false
	if strings.HasPrefix(name, "-") {
		name = name[1:]
		is_minus = true
	}
	confs.LOG.Info.Println(name)
	tags, _ := models.FindTags(db, name, size, page)
	if is_minus {
		for i := 0; i < len(tags); i++ {
			tags[i].Name = "-" + tags[i].Name
		}
	}
	b, err := json.Marshal(tags)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	w.Write(b)

}

func GetTag(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	id, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	bid := bson.ObjectIdHex(id)
	tag := new(models.Tag)
	err := models.FindTagId(db, bid, tag)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	b, err := json.Marshal(tag)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	w.Write(b)

}
