package rest

import (
	"encoding/json"
	"gorecipes/confs"
	"gorecipes/models"
	"gorecipes/utils"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

func ListIngredients(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	size, page := utils.SizePage(r)
	name := r.FormValue("Name")
	is_minus := false
	if strings.HasPrefix(name, "-") {
		name = name[1:]
		is_minus = true
	}
	ings, _ := models.FindIngredients(db, name, size, page)
	if is_minus {
		for i := 0; i < len(ings); i++ {
			ings[i].Name = "-" + ings[i].Name
		}
	}

	b, err := json.Marshal(ings)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	w.Write(b)
}

func SearchIngredients(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	data := struct {
		Name string
		Tags []string
	}{}

	d := json.NewDecoder(r.Body)
	err := d.Decode(&data)
	if err != nil {
		confs.LOG.Error.Println(err.Error())

		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	size, page := utils.SizePage(r)

	tags, _ := models.NamesToTags(db, data.Tags)
	ings, _ := models.SearchIngredients(db, data.Name, tags, size, page)

	b, err := json.Marshal(ings)
	if err != nil {
		confs.LOG.Error.Println(err.Error())
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	w.Write(b)

}

func GetIngredient(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	id, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	bid := bson.ObjectIdHex(id)
	ing := new(models.Ingredient)
	err := models.FindIngredientId(db, bid, ing)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	stags := []string{}
	for _, v := range ing.Tags {
		tag := new(models.Tag)
		models.FindTagId(db, v, tag)
		stags = append(stags, tag.Name)
	}

	ingm := map[string]interface{}{
		"Id":       ing.Id,
		"Name":     ing.Name,
		"STags":    stags,
		"Tags":     ing.Tags,
		"HasPhoto": ing.HasPhoto,
	}
	b, err := json.Marshal(ingm)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	w.Write(b)
}

func GetProductsByIngredient(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	id, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	bid := bson.ObjectIdHex(id)
	ing := new(models.Ingredient)
	err := models.FindIngredientId(db, bid, ing)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	products, err := ing.GetProducts(db)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	cookie, _ := utils.GetCookieId(r)
	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		http.Error(w, "no cookie", http.StatusNotFound)
		return
	}

	for i, v := range products {
		if cobj.IsChoosenProduct(db, v.Id) {
			products[i].IsChoosen = true
		}
	}

	b, err := json.Marshal(products)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	w.Write(b)

}
