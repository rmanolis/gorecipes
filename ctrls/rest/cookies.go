package rest

import (
	"encoding/json"
	"gorecipes/confs"
	"gorecipes/models"
	"gorecipes/utils"

	"net/http"

	"gopkg.in/mgo.v2"
)

func createNewCookie(w http.ResponseWriter, r *http.Request, db *mgo.Database) *models.Cookie {
	confs.LOG.Info.Println("Create new cookie")
	cookie := new(models.Cookie)

	err := cookie.Insert(db)
	if err != nil {
		confs.LOG.Error.Println(err.Error())
	} else {
		confs.LOG.Info.Println(cookie.Id)
	}
	utils.SetSession(w, r, "cookie_id", cookie.Id.Hex())
	return cookie
}

func GetCookie(w http.ResponseWriter, r *http.Request) {
	confs.LOG.Info.Println("New cookie")
	db := utils.GetDB(r)
	_, err := utils.GetCookieId(r)
	if err != nil {
		createNewCookie(w, r, db)
	}

}

func SaveCookie(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	str, err := utils.GetCookieId(r)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	cookie, err := models.FindCookie(db, str)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	d := json.NewDecoder(r.Body)
	u := struct {
		Name string
	}{}
	err = d.Decode(&u)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	cookie.IsSaved = true
	cookie.Update(db)

	createNewCookie(w, r, db)
	w.Write([]byte(cookie.Id.Hex()))
}

func CancelCookie(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	str, err := utils.GetCookieId(r)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	cookie, err := models.FindCookie(db, str)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	cookie.IsCanceled = true
	cookie.Remove(db)

	createNewCookie(w, r, db)
}
