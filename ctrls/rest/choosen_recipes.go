package rest

import (
	"encoding/json"
	"gorecipes/confs"
	"gorecipes/models"
	"gorecipes/utils"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

func GetChoosenRecipes(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	cookie, err := utils.GetCookieId(r)
	if err != nil {
		confs.LOG.Error.Println(err.Error())
		http.Error(w, "no cookie", http.StatusInternalServerError)
		return
	}

	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		confs.LOG.Error.Println(err.Error())
		http.Error(w, "no cookie", http.StatusNotFound)
		return
	}

	recs := cobj.GetRecipes(db)
	for i, v := range recs {
		recs[i].IsChoosen = true
		sings := ""
		for _, ing := range v.GetIngredients(db) {
			sings += ing.Name + ", "
		}
		recs[i].SIngredients = sings
		stags := ""
		for _, tag := range v.GetTags(db) {
			stags += tag.Name + ", "
		}
		recs[i].STags = stags
		if v.UserId.Valid() {
			user, err := models.GetUserById(db, v.UserId)
			if err == nil {
				recs[i].UserName = user.Name
			}
		}

	}

	b, err := json.Marshal(recs)
	if err != nil {
		confs.LOG.Error.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(b)
}

func GetProductsByChoosenRecipes(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	cookie, err := utils.GetCookieId(r)
	if err != nil {
		http.Error(w, "no cookie", http.StatusInternalServerError)
		return
	}

	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		http.Error(w, "no cookie", http.StatusInternalServerError)
		return
	}

	prods, _ := cobj.GetProductsByRecipes(db)

	for i, v := range prods {
		if cobj.IsChoosenProduct(db, v.Id) {
			prods[i].IsChoosen = true
		}
	}

	b, err := json.Marshal(prods)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	w.Write(b)

}

func GetIngredientsByChoosenRecipes(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	cookie, err := utils.GetCookieId(r)
	if err != nil {
		http.Error(w, "no cookie", http.StatusInternalServerError)
		return
	}

	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		http.Error(w, "no cookie", http.StatusNotFound)
		return
	}

	ings, _ := cobj.GetIngredientsByRecipes(db)
	b, err := json.Marshal(ings)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	w.Write(b)

}

func ChooseRecipe(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	cookie, err := utils.GetCookieId(r)
	if err != nil {
		http.Error(w, "no cookie", http.StatusInternalServerError)
		return
	}

	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		http.Error(w, "no cookie", http.StatusNotFound)
		return
	}

	id, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	recipe := new(models.Recipe)
	err = models.FindRecipeId(db, bson.ObjectIdHex(id), recipe)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	cr := new(models.ChoosenRecipe)
	cr.CookieId = cobj.Id
	cr.RecipeId = recipe.Id
	err = cr.Insert(db)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

}

func RejectRecipe(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	cookie, err := utils.GetCookieId(r)
	if err != nil {
		http.Error(w, "no cookie", http.StatusInternalServerError)
		return
	}

	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		http.Error(w, "no cookie", http.StatusNotFound)
		return
	}

	id, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	cr := new(models.ChoosenRecipe)
	err = models.FindChoosenRecipe(db, cobj.Id, bson.ObjectIdHex(id), cr)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	err = cr.Remove(db)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

}
