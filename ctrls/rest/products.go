package rest

import (
	"encoding/json"
	"gorecipes/models"
	"gorecipes/utils"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

func SearchProducts(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	data := struct {
		Name        string
		Tags        []string
		Ingredients []string
	}{}

	d := json.NewDecoder(r.Body)
	err := d.Decode(&data)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	size, page := utils.SizePage(r)
	tags, _ := models.NamesToTags(db, data.Tags)
	ings, _ := models.NamesToIngredients(db, data.Ingredients)
	prods, _ := models.SearchProducts(db, data.Name, ings, tags, size, page)

	cookie, _ := utils.GetCookieId(r)
	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	for i, v := range prods.Objects.([]models.Product) {
		if cobj.IsChoosenProduct(db, v.Id) {
			prods.Objects.([]models.Product)[i].IsChoosen = true
		}
	}
	b, err := json.Marshal(prods)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	w.Write(b)
}

func ListProducts(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	size, page := utils.SizePage(r)
	name := r.FormValue("Name")
	prods, _ := models.FindProducts(db, name, size, page)
	cookie, _ := utils.GetCookieId(r)
	cobj, err := models.FindCookie(db, cookie)
	if err == nil {
		for i, v := range prods {
			if cobj.IsChoosenProduct(db, v.Id) {
				prods[i].IsChoosen = true
			}
		}
	}
	b, err := json.Marshal(prods)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	w.Write(b)
}

func GetProduct(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	id, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	bid := bson.ObjectIdHex(id)
	prod := new(models.Product)
	err := models.FindProductId(db, bid, prod)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	stags := []string{}
	for _, v := range prod.Tags {
		tag := new(models.Tag)
		models.FindTagId(db, v, tag)
		stags = append(stags, tag.Name)
	}

	sings := []string{}
	for _, v := range prod.Ingredients {
		ing := new(models.Ingredient)
		models.FindIngredientId(db, v, ing)
		sings = append(sings, ing.Name)
	}
	prodm := map[string]interface{}{
		"Id":           prod.Id,
		"Name":         prod.Name,
		"ProductType":  prod.ProductType,
		"Description":  prod.Description,
		"Price":        prod.Price,
		"STags":        stags,
		"Tags":         prod.Tags,
		"SIngredients": sings,
		"Ingredients":  prod.Ingredients,
		"HasPhoto":     prod.HasPhoto,
	}
	cookie, _ := utils.GetCookieId(r)
	cobj, err := models.FindCookie(db, cookie)
	if err == nil {
		if cobj.IsChoosenProduct(db, prod.Id) {
			prodm["IsChoosen"] = true
		}
	}
	b, err := json.Marshal(prodm)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	w.Write(b)

}
