package rest

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"gorecipes/confs"
	"gorecipes/models"
	"gorecipes/utils"
	"net/http"
)

func SearchRecipes(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	data := struct {
		Name        string
		Tags        []string
		Ingredients []string
		ShowAll     bool
	}{}
	d := json.NewDecoder(r.Body)
	err := d.Decode(&data)
	confs.LOG.Info.Println(data)
	if err != nil {
		confs.LOG.Error.Println(err.Error())
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	size, page := utils.SizePage(r)
	add_tags, rem_tags := models.NamesToTags(db, data.Tags)
	add_ings, rem_ings := models.NamesToIngredients(db, data.Ingredients)
	recs, err := models.SearchRecipes(db, data.Name, add_ings, rem_ings, add_tags, rem_tags, data.ShowAll, size, page)
	if err != nil {
		confs.LOG.Error.Println(err.Error())
	}
	cookie, _ := utils.GetCookieId(r)
	cobj, err := models.FindCookie(db, cookie)
	if err != nil {
		confs.LOG.Error.Println(err.Error())
		cobj = createNewCookie(w, r, db)
	}

	for i, v := range recs.Objects.([]models.Recipe) {
		if cobj != nil {
			if cobj.IsChoosenRecipe(db, v.Id) {
				recs.Objects.([]models.Recipe)[i].IsChoosen = true
			}
		}
		sings := ""
		for _, ing := range v.GetIngredients(db) {
			sings += ing.Name + ", "
		}
		recs.Objects.([]models.Recipe)[i].SIngredients = sings
		stags := ""
		for _, tag := range v.GetTags(db) {
			stags += tag.Name + ", "
		}
		recs.Objects.([]models.Recipe)[i].STags = stags
		if v.UserId.Valid() {
			user, err := models.GetUserById(db, v.UserId)
			if err == nil {
				recs.Objects.([]models.Recipe)[i].UserName = user.Name
			}
		}
	}
	b, err := json.Marshal(recs)
	if err != nil {
		confs.LOG.Error.Println(err.Error())
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	w.Write(b)

}

func ListRecipes(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	size, page := utils.SizePage(r)
	name := r.FormValue("Name")
	recs, _ := models.FindRecipes(db, name, size, page)
	cookie, _ := utils.GetCookieId(r)
	cobj, err := models.FindCookie(db, cookie)
	if err == nil {
		for i, v := range recs {
			if cobj.IsChoosenRecipe(db, v.Id) {
				recs[i].IsChoosen = true
			}
		}
	}
	b, err := json.Marshal(recs)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	w.Write(b)

}

func GetRecipe(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	id, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	bid := bson.ObjectIdHex(id)
	rec := new(models.Recipe)
	err := models.FindRecipeId(db, bid, rec)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	recm := getJsonRecipe(db, rec)
	cookie, _ := utils.GetCookieId(r)

	cobj, err := models.FindCookie(db, cookie)

	if err == nil {
		if cobj.IsChoosenRecipe(db, rec.Id) {
			recm["IsChoosen"] = true
		}
	}
	b, err := json.Marshal(recm)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	w.Write(b)

}

func GetIngredientsByRecipe(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)

	id, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	bid := bson.ObjectIdHex(id)
	rec := new(models.Recipe)
	err := models.FindRecipeId(db, bid, rec)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	ings := rec.GetIngredients(db)
	b, err := json.Marshal(ings)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	w.Write(b)

}

func getJsonRecipe(db *mgo.Database, rec *models.Recipe) map[string]interface{} {
	stags := []string{}
	for _, v := range rec.Tags {
		tag := new(models.Tag)
		models.FindTagId(db, v, tag)
		stags = append(stags, tag.Name)
	}

	sings := []string{}
	for _, v := range rec.Ingredients {
		ing := new(models.Ingredient)
		models.FindIngredientId(db, v, ing)
		sings = append(sings, ing.Name)
	}

	sprods := []string{}
	for _, v := range rec.Products {
		product := new(models.Product)
		models.FindProductId(db, v, product)
		sprods = append(sprods, product.Name)
	}

	recm := map[string]interface{}{
		"Id":           rec.Id,
		"Name":         rec.Name,
		"Servings":     rec.Servings,
		"Steps":        rec.Steps,
		"STags":        stags,
		"Tags":         rec.Tags,
		"SIngredients": sings,
		"Ingredients":  rec.Ingredients,
		"SProducts":    sprods,
		"Products":     rec.Products,
		"IsChoosen":    rec.IsChoosen,
		"HasPhoto":     rec.HasPhoto,
	}

	return recm
}
