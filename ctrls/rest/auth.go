package rest

import (
	"encoding/json"
	"gorecipes/confs"
	"gorecipes/models"
	"gorecipes/utils"
	"net/http"
)

func IsUser(w http.ResponseWriter, r *http.Request) {
	_, err := utils.GetUserId(r)
	if err != nil {
		http.Error(w, "", http.StatusUnauthorized)
		return
	}
}

func IsAdmin(w http.ResponseWriter, r *http.Request) {
	_, err := utils.GetAdminId(r)
	if err != nil {
		http.Error(w, "", http.StatusUnauthorized)
		return
	}
}

func Login(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	user := new(models.User)
	d := json.NewDecoder(r.Body)
	err := d.Decode(user)
	if err != nil {
		confs.LOG.Info.Println(err.Error())
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	err = user.Authenticate(db)
	if err != nil {
		confs.LOG.Info.Println(err.Error())
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	utils.SetSession(w, r, "user_id", user.Id.Hex())
	if user.IsAdmin {
		utils.SetSession(w, r, "admin_id", user.Id.Hex())
	}
}

func Logout(w http.ResponseWriter, r *http.Request) {
	utils.DeleteCookies(w, r)
}
