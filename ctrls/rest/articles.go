package rest

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"gorecipes/models"
	"gorecipes/utils"
	"net/http"
)

func SearchArticles(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)

	data := struct {
		Title string
		Text  string
	}{}

	d := json.NewDecoder(r.Body)
	err := d.Decode(&data)

	size, page := utils.SizePage(r)
	articles, _ := models.SearchArticles(db, data.Title, data.Text, size, page)
	b, err := json.Marshal(articles)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	w.Write(b)
}

func GetArticle(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	vars := mux.Vars(r)
	link := vars["link"]
	article := new(models.Article)
	err := models.FindArticleByLink(db, link, article)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	b, err := json.Marshal(article)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	w.Write(b)
}
