package admin

import (
	"encoding/json"
	"gorecipes/models"
	"gorecipes/utils"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func ShowRecipes(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, r, "admin/recipes", nil)
}

func ShowEditRecipe(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, r, "admin/recipe", nil)
}

func setRecipe(r *http.Request,
	db *mgo.Database, rec *models.Recipe) {
	if !rec.Id.Valid() {
		rec.Id = bson.NewObjectId()
	}

	rec.Name = r.FormValue("Name")
	rec.Servings = r.FormValue("Servings")
	rec.Steps = r.FormValue("Steps")
	tags := []string{}
	st := r.FormValue("Tags")
	json.Unmarshal([]byte(st), &tags)
	rec.Tags, _ = models.NamesToTags(db, tags)

	ings := []string{}
	si := r.FormValue("Ingredients")
	json.Unmarshal([]byte(si), &ings)
	rec.Ingredients, _ = models.NamesToIngredients(db, ings)

	prods := []string{}
	sp := r.FormValue("Products")
	json.Unmarshal([]byte(sp), &prods)
	rec.Products = models.NamesToProducts(db, prods)
	err := utils.SaveFile(r, "static/photos/r_"+rec.Id.Hex()+".jpg")
	if err == nil {
		rec.HasPhoto = true
	}
	rec.Language = models.Language(r.FormValue("Language"))
}

func AddRecipe(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	id, err := utils.GetUserId(r)
	if err != nil {
		http.Error(w, "", http.StatusUnauthorized)
		return
	}
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusUnauthorized)
		return
	}
	rec := new(models.Recipe)
	setRecipe(r, db, rec)
	rec.UserId = bson.ObjectIdHex(id)
	rec.IsApproved = false
	rec.Language = models.GREEK
	err = rec.Insert(db)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

}

func EditRecipe(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	id, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	rec := new(models.Recipe)
	err := models.FindRecipeId(db, bson.ObjectIdHex(id), rec)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	setRecipe(r, db, rec)
	err = rec.Update(db)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

}

func ApproveRecipe(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	id, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	rec := new(models.Recipe)
	err := models.FindRecipeId(db, bson.ObjectIdHex(id), rec)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	rec.IsApproved = !rec.IsApproved
	err = rec.Update(db)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
}

func DeleteRecipe(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	id, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	rec := new(models.Recipe)
	err := models.FindRecipeId(db, bson.ObjectIdHex(id), rec)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if rec.HasPhoto {
		utils.DeleteFile("static/photos/r_" + rec.Id.Hex() + ".jpg")
	}

	err = rec.Remove(db)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
}

func DeleteFileRecipe(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	id, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	rec := new(models.Recipe)
	err := models.FindRecipeId(db, bson.ObjectIdHex(id), rec)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if !rec.HasPhoto {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	err = utils.DeleteFile("static/photos/r_" + rec.Id.Hex() + ".jpg")
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	rec.HasPhoto = false
	rec.Update(db)
}

func ListRecipesByUser(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	size, page := utils.SizePage(r)
	name := r.FormValue("Name")
	user_id, err := utils.GetUserId(r)
	if err != nil {
		http.Error(w, "", http.StatusUnauthorized)
		return
	}
	if !bson.IsObjectIdHex(user_id) {
		http.Error(w, "", http.StatusUnauthorized)
		return
	}
	user, err := models.GetUserById(db, bson.ObjectIdHex(user_id))
	if err != nil {
		http.Error(w, "", http.StatusUnauthorized)
		return
	}

	recs, _ := user.FindRecipes(db, name, size, page)
	cookie, _ := utils.GetCookieId(r)
	cobj, err := models.FindCookie(db, cookie)
	if err == nil {
		for i, v := range recs.Objects.([]models.Recipe) {
			if cobj.IsChoosenRecipe(db, v.Id) {
				recs.Objects.([]models.Recipe)[i].IsChoosen = true
			}
			sings := ""
			for _, ing := range v.GetIngredients(db) {
				sings += ing.Name + ", "
			}
			recs.Objects.([]models.Recipe)[i].SIngredients = sings
			stags := ""
			for _, tag := range v.GetTags(db) {
				stags += tag.Name + ", "
			}
			recs.Objects.([]models.Recipe)[i].STags = stags
			if v.UserId.Valid() {
				user, err := models.GetUserById(db, v.UserId)
				if err == nil {
					recs.Objects.([]models.Recipe)[i].UserName = user.Name
				}
			}
		}
	}
	b, err := json.Marshal(recs)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	w.Write(b)

}
