package admin

import (
	"gorecipes/confs"
	"gorecipes/models"
	"gorecipes/utils"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func setArticle(r *http.Request,
	db *mgo.Database, art *models.Article) {
	if !art.Id.Valid() {
		art.Id = bson.NewObjectId()
	}

	art.Title = r.FormValue("Title")
	art.Link = r.FormValue("Link")
	art.Text = r.FormValue("Text")
	err := utils.SaveFile(r, "static/photos/a_"+art.Id.Hex()+".jpg")
	if err == nil {
		art.HasPhoto = true
	}
}

func ShowArticles(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, r, "admin/articles", nil)

}

func ShowEditArticle(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, r, "admin/article", nil)
}

func AddArticle(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	user_id, _ := utils.GetUserId(r)
	art := new(models.Article)
	setArticle(r, db, art)
	if !bson.IsObjectIdHex(user_id) {
		http.Error(w, "", http.StatusUnauthorized)
		return
	}
	art.UserId = bson.ObjectIdHex(user_id)
	err := art.Insert(db)
	if err != nil {
		confs.LOG.Error.Println(err.Error())
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

}

func EditArticle(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	user_id, _ := utils.GetUserId(r)
	link, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	art := new(models.Article)
	err := models.FindArticleByLink(db, link, art)
	if err != nil {
		confs.LOG.Error.Println(err.Error())
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	setArticle(r, db, art)
	if !bson.IsObjectIdHex(user_id) {
		http.Error(w, "", http.StatusUnauthorized)
		return
	}
	art.UserId = bson.ObjectIdHex(user_id)
	err = art.Update(db)
	if err != nil {
		confs.LOG.Error.Println(err.Error())
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
}

func DeleteArticle(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	link, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	art := new(models.Article)
	err := models.FindArticleByLink(db, link, art)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	if art.HasPhoto {
		utils.DeleteFile("static/photos/a_" + art.Id.Hex() + ".jpg")
	}
	err = art.Remove(db)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

}

func DeleteFileArticle(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	link, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	art := new(models.Article)
	err := models.FindArticleByLink(db, link, art)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	if !art.HasPhoto {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	err = utils.DeleteFile("static/photos/a_" + art.Id.Hex() + ".jpg")
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	art.HasPhoto = false
	art.Update(db)
}
