package admin

import (
	"encoding/json"
	"gorecipes/models"
	"gorecipes/utils"
	"net/http"

	"github.com/gorilla/mux"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"strconv"
)

func setProduct(r *http.Request, db *mgo.Database, prod *models.Product) error {
	if !prod.Id.Valid() {
		prod.Id = bson.NewObjectId()
	}

	prod.Name = r.FormValue("Name")
	prod.ProductType = r.FormValue("ProductType")
	prod.Description = r.FormValue("Description")
	price, err := strconv.ParseFloat(r.FormValue("Price"), 64)
	if err != nil {
		return err
	}
	prod.Price = price
	tags := []string{}
	st := r.FormValue("Tags")
	json.Unmarshal([]byte(st), &tags)
	prod.Tags, _ = models.NamesToTags(db, tags)

	ings := []string{}
	si := r.FormValue("Ingredients")
	json.Unmarshal([]byte(si), &ings)
	prod.Ingredients, _ = models.NamesToIngredients(db, ings)
	err = utils.SaveFile(r, "static/photos/p_"+prod.Id.Hex()+".jpg")
	if err == nil {
		prod.HasPhoto = true
	}
	prod.Language = models.Language(r.FormValue("Language"))
	return nil
}

func ShowProducts(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, r, "admin/products", nil)
}

func ShowEditProduct(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, r, "admin/product", nil)
}

func AddProduct(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	prod := new(models.Product)
	err := setProduct(r, db, prod)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	err = prod.Insert(db)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
}

func EditProduct(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	id, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	prod := new(models.Product)
	err := models.FindProductId(db, bson.ObjectIdHex(id), prod)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	err = setProduct(r, db, prod)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	err = prod.Update(db)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

}

func DeleteProduct(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	id, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	prod := new(models.Product)
	err := models.FindProductId(db, bson.ObjectIdHex(id), prod)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if prod.HasPhoto {
		utils.DeleteFile("static/photos/p_" + prod.Id.Hex() + ".jpg")
	}

	err = prod.Remove(db)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

}

func DeleteFileProduct(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	id, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	prod := new(models.Product)
	err := models.FindProductId(db, bson.ObjectIdHex(id), prod)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if !prod.HasPhoto {
		http.Error(w, "", http.StatusNotFound)
		return

	}
	err = utils.DeleteFile("static/photos/p_" + prod.Id.Hex() + ".jpg")
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	prod.HasPhoto = false
	prod.Update(db)

}
