package admin

import (
	"gorecipes/confs"
	"gorecipes/models"
	"gorecipes/utils"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

func ShowTags(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, r, "admin/tags", nil)
}

func ShowEditTag(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, r, "admin/tag", nil)
}

func AddTag(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	tag := new(models.Tag)
	tag.Name = r.FormValue("Name")
	tag.Language = models.GREEK
	err := tag.Insert(db)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
}

func EditTag(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	id, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return

	}
	tag := new(models.Tag)
	err := models.FindTagId(db, bson.ObjectIdHex(id), tag)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	tag.Name = r.FormValue("Name")
	confs.LOG.Info.Println(tag.Name)
	err = tag.Update(db)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return

	}
}

func DeleteTag(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	id, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return

	}
	tag := new(models.Tag)
	err := models.FindTagId(db, bson.ObjectIdHex(id), tag)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	err = tag.Remove(db)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return

	}
}
