package admin

import (
	"encoding/json"
	"gorecipes/models"
	"gorecipes/utils"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

func ShowUsers(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, r, "admin/users", nil)
}

func ShowOneUser(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, r, "admin/user", nil)
}

func ListCookiesOfUser(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	id, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	bid := bson.ObjectIdHex(id)
	user, err := models.GetUserById(db, bid)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	size, page := utils.SizePage(r)
	ls, _ := user.GetSavedCookies(db, size, page)
	b, err := json.Marshal(ls)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	w.Write(b)
}

func SearchUsers(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	size, page := utils.SizePage(r)
	name := r.FormValue("Name")
	email := r.FormValue("Email")
	users, _ := models.SearchUsers(db, name, email, size, page)
	b, err := json.Marshal(users)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	w.Write(b)

}
