package admin

import (
	"encoding/json"
	"gorecipes/models"
	"gorecipes/utils"
	"net/http"

	"github.com/gorilla/mux"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func ShowIngredients(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, r, "admin/ingredients", nil)
}

func ShowEditIngredient(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, r, "admin/ingredient", nil)
}

func setIngredient(r *http.Request,
	db *mgo.Database, ing *models.Ingredient) error {
	if !ing.Id.Valid() {
		ing.Id = bson.NewObjectId()
	}
	ing.Language = models.Language(r.FormValue("Language"))
	ing.Name = r.FormValue("Name")
	tags := []string{}
	st := r.FormValue("Tags")
	err := json.Unmarshal([]byte(st), &tags)
	if err != nil {
		return err
	}

	ing.Tags, _ = models.NamesToTags(db, tags)
	err = utils.SaveFile(r, "static/photos/i_"+ing.Id.Hex()+".jpg")
	if err == nil {
		ing.HasPhoto = true
	}
	return nil
}

func AddIngredient(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	ing := new(models.Ingredient)
	err := setIngredient(r, db, ing)
	if err != nil {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}

	ing.Language = models.GREEK
	err = ing.Insert(db)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
}

func EditIngredient(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	id, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	ing := new(models.Ingredient)
	err := models.FindIngredientId(db, bson.ObjectIdHex(id), ing)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	ing.Name = r.FormValue("Name")
	err = setIngredient(r, db, ing)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	err = ing.Update(db)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

}

func DeleteIngredient(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	id, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	ing := new(models.Ingredient)
	err := models.FindIngredientId(db, bson.ObjectIdHex(id), ing)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if ing.HasPhoto {
		utils.DeleteFile("static/photos/i_" + ing.Id.Hex() + ".jpg")
	}

	err = ing.Remove(db)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
}

func DeleteFileIngredient(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	id, ok := mux.Vars(r)["id"]
	if !ok {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	ing := new(models.Ingredient)
	err := models.FindIngredientId(db, bson.ObjectIdHex(id), ing)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if !ing.HasPhoto {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	err = utils.DeleteFile("static/photos/i_" + ing.Id.Hex() + ".jpg")
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	ing.HasPhoto = false
	ing.Update(db)
}
