package admin

import (
	"encoding/csv"
	"gorecipes/models"
	"gorecipes/utils"
	"log"
	"net/http"
	"time"
)

func ShowStatistics(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, r, "admin/statistics", nil)
}

func SearchStatistics(w http.ResponseWriter, r *http.Request) {
	db := utils.GetDB(r)
	start_s := r.FormValue("Start")
	end_s := r.FormValue("End")
	start, err := time.Parse("02/01/2006", start_s)
	if err != nil {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}

	end, err := time.Parse("02/01/2006", end_s)
	if err != nil {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}

	on := r.FormValue("On")
	if on == "recipes" {
		sheet := models.GetMatrixChoosenRecipes(db, start, end)
		log.Println(sheet.ToString())

		writer := csv.NewWriter(w)
		for _, record := range sheet.ToString() {
			err := writer.Write(record)
			if err != nil {
				http.Error(w, "", http.StatusInternalServerError)
				return
			}
		}
		w.Header().Set("Content-Disposition", "attachment; filename=recipes.csv")
		w.Header().Set("Content-Type", "text/csv")
		writer.Flush()
	} else if on == "products" {
		sheet := models.GetMatrixChoosenProducts(db, start, end)

		writer := csv.NewWriter(w)
		for _, record := range sheet.ToString() {
			err := writer.Write(record)
			if err != nil {
				http.Error(w, "", http.StatusInternalServerError)
				return
			}
		}
		w.Header().Set("Content-Disposition", "attachment; filename=products.csv")
		w.Header().Set("Content-Type", "text/csv")
		writer.Flush()
	} else {
		http.Error(w, "", http.StatusNotFound)
	}

}
