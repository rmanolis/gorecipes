- Refactor the code
  + remove user id from cookie
  - remove pdf
  - Rename Cookie to GroceryList
  - remove golang code and templates for products, articles


- test add and delete picture
- show picture when uploading before adding the recipe
- show picture when editing recipe and change it to the new one

- for all the pages that add picture
+ fix web design for register and login
- split admins from users
+ add tags and ingredients
- text editor that allows "enter"
- when adding note, press enter for the "ok"

- create page for other user to see the recipes that other people submited 
- create tables for the admin panel

- Web design mobile
  - the base page will have a navigation menu 
- translations
- FIX bugs 
  - From Statistics it does not count correctly

#FOR BETA
Mobile App
  - Login 
  - Logout button  
  - Saved Grocery Lists
        - Show the grocery list online and offline are synced 
        - If something deleted offline save on table "deleted" the id 
        So when online, to remove this id from the db
        - If something deleted online , then when the mobile finds the missed IDs and deletes them
+ When searching tags and ingredients , 
  then add '-' front of the words to show recipes and products without it
- Enable showing Products from  basket, recipe page, grocery list


#NOT YET IMPORTANT  
- In the list of users show, if shop owner, admin or nothing

- Model Shop [Name, description, UserId]
  - Will have a list of locations by shop {Address, Phone, Email, Location}
  - Will have a list of products

- User from settings enables shop owner
- If shop owner enabled, create a new navigation
  - Edit Shop
    - Edit Name, Description
  - List Locations [Address, Geolocation, Phone]
  - List his Products  
    - Add, Edit, Delete Product
  - Statistics
  - List deliveries

- Rather than τελειωσε , use "save" or "cancel" 

- Button to buy choosen products if shop supports delivery

- If user is 1 km closer to a shop, show only the products from that super market.

- Admin list of users have a button to make admin

- For users
  - comments to recipes
  - add recipes

##Past finished tasks
Pages 
  + replace all the SSP lists to more dynamic ones with search
  + create admin folder for js controllers
  + List Users and search by name and email
  + List cookies (remove this page)
    + by date
  + For each user show :
     + list paginated cookies by date and choose to list:
        + list choosen products
        + list ingrediens
        + list products by choosen recipes
  + Add, Edit Image for Ingredient, Recipe and Product with Id for name
  + Ingredients List, Add, Edit, Delete
  + Tag   List, Add, Edit, Delete
  + Recipe List, Add, Edit, Delete
    + Add , Edit Products
  + Product List, Add, Edit, Delete
    + Add, Edit with product type
  + Page for statistics 
   + Send a CSV from the total number of choosen recipes and products by day

+ Nav menu
  + Admins will have CRUD on Recipes, Ingredients, Tags and Products
    For adding Ingredients , Tags will use autocomplete and an add button.

+ Search Recipes
  + Form:
    + By name or
    + By ingredients or
    + By Tags

  + List Recipes
    + Each recipe has a check box
    + Title
    + description
    + pages
    + is checked

  + if just one recipe is checked, then it will show a button to show the cookie's choices


+ List of ingredients and choosen recipes
  + list choosen recipes
  + list ingredients
  + show products for each ingredient and choose
  + show products from each recipe and choose
  + list of choosen products
  + Button to create a pdf
  + Finish button to recreate the cookie
    + If they are not users, tell them to register or login

+ Page for the recipes and products that are SSP for SEO

+ Page to change user's password

+ Front page show latest recipes and products

+ Add choose button to index and single items

+ Seperate in a template the baskets of choosen recipes and products.
  They will be in any list or single pages of recipes and products

+ Add text editor for recipes and products
  + Make sure that the templates will take the html of the text editors

+ Change architect of the web site
  + Use angular rather than knockout, but dont use the router of angular. 
    However change the templates {{}} of server side
    beego.TemplateLeft = "<<<"
    beego.TemplateRight = ">>>"
+ Cancel or Save (if user) cookie

+ Model articles [Title,LinkName, Text, UserId, Date]
  + CRUD over /articles/:LinkName 
  
+ Admin tag list to Single page

+ For users
  + add pictures
 

+ For users, list the saved grocery lists by date 
    + and another page to show the grocery list

Mobile App
  + Search Recipes
  + Current Grocery list
    + save button local 
  + Saved Grocery Lists
    + Shows the grocery lists offline with delete button
  + Saved grocery list
      + each ingredient and products will have two fields state and end_date.
        State will have "miss", "bought", "need"
        End_Date the user will edit it when change the state from miss to bought
        If he wants, he can leave the end_date empty
  + Use Forerunner DB with IndexedDB
  


+ Remove from mobile APP any api that has to do with user. 


+ Delete picture for every model

+ Disable showing Products from index, basket, recipes, nav and pdf

+ Sketch Web and Mobile

+ Make pretty the buttons "Choose" and "Reject"
+ Fix images and default one
+ On the recipe lists each recipe will have user's name, list of ingredients and the date
+ Page for each recipe
+ Basket will have two buttons "Save" and "Cancel"
+ Grocery lists will be saved on the browser. 
+ Grocery lists will have their own page
  + split in two sections one for listing the names and the other showing them
+ A page for showing user's recipes
+ A page for user to submit recipes

+ admin approves user's recipe
 
+ buy url
+ create logo

