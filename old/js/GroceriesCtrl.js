app.controller('GroceriesCtrl',
    ["$scope","$uibModal", "UserSrv","RecipeSrv","IngredientSrv","ProductSrv",
    "PDFSrv","CookieSrv",
    function($scope, $uibModal,UserSrv, RecipeSrv, IngredientSrv, ProductSrv, PDFSrv, CookieSrv) {
      $scope.recipes  =[];
      $scope.ingredients  = [];
      $scope.recipeProducts  = [];
      $scope.ingredientProducts  = [];
      $scope.choosenProducts  = [];
      $scope.isUser = false;
      UserSrv.isUser().success(function(){
        $scope.isUser=true;
      });

      function updateRecipes(){
        RecipeSrv.getChoosenRecipes().success(function(crs){
          console.log(crs);
          $scope.recipes=crs;
        });
        IngredientSrv.getIngredientsByChoosenRecipes().success(function(ings){
          console.log(ings);
          $scope.ingredients = ings;
        });
        ProductSrv.getProductsByChoosenRecipes().success(function(prods){
          console.log(prods);
          $scope.recipeProducts = prods;
        });
      }

      function updateProducts(){
        ProductSrv.getChoosenProducts().success(function(prods){
          $scope.choosenProducts=prods;
        });
      }

      $scope.showProductsByIngredient = function(id){
        ProductSrv.getProductsByIngredient(id).success(function(ings){
          console.log(ings);
          $scope.ingredientProducts = ings;
        });
      }

      $scope.rejectRecipe = function(id){
        RecipeSrv.rejectRecipe(id).success(function(){
          console.log("rejected");        
          updateRecipes();
        });
      }

      $scope.chooseProduct = function(id){
        console.log(id);
        ProductSrv.chooseProduct(id).success(function(){
          console.log("added");
          updateIngredientProducts(id, true);
          updateRecipeProducts(id,true);
          updateProducts();
        });

      }

      $scope.rejectProduct = function(id){
        ProductSrv.rejectProduct(id).success(function(){
          console.log("removed");
          updateIngredientProducts(id, false);
          updateRecipeProducts(id,false);
          updateProducts();
        });
      }

      $scope.rejectAllProducts = function(){
        ProductSrv.getChoosenProducts().success(function(crs){
          for(i=0; i < crs.length ; i++ ){
            ProductSrv.rejectProduct(crs[i].Id);
          }
          updateIngredientProducts(null, false);
          updateRecipeProducts(null,false);

          updateProducts();
        });
      }

      $scope.rejectProduct = function(id){
        ProductSrv.rejectProduct(id).success(function(){
          console.log("removed");
          updateIngredientProducts(id,false);
          updateRecipeProducts(id,false);
          updateProducts();
        });
      }

      $scope.createPDF = function(){
        PDFSrv.createPDF();
      }



      function updateIngredientProducts(id, b){
        $.each($scope.ingredientProducts, function (i,obj) {
          if(obj.Id == id){
            obj.IsChoosen = b;
          }else if(id == null){
            obj.IsChoosen = b;
          }
          $scope.$apply();
        });
      }

      function updateRecipeProducts(id,b){
        $.each($scope.recipeProducts, function (i,obj) {
          if(obj.Id == id){
            obj.IsChoosen = b;
          }else if(id == null){
            obj.IsChoosen = b;
          }
          $scope.$apply();
        });
      }


      updateRecipes();
      updateProducts();


      $scope.toSave = function () {
        UserSrv.isUser().success(function(){
          var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'myModalContent.html',
            controller: 'ModalSaveCtrl',
            size: 'sm',
            resolve: {
            }
          });
          modalInstance.result.then(function (name) {
            CookieSrv.saveCookie(name).success(function(){
              window.location.pathname = "/recipes/search";
            })
          }, function () {
          });
        }).error(function(){
          alert("You have to be a user");
        })
      }

      $scope.toCancel = function(){
        CookieSrv.cancelCookie().success(function(){
          window.location.pathname = "/recipes/search";
        })
      }



    }]);


app.controller('ModalSaveCtrl', function ($scope, $uibModalInstance) {



  $scope.ok = function () {
    $uibModalInstance.close($scope.name);
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});

