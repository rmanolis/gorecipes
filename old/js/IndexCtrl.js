app.controller('IndexCtrl',
    ["$scope","$rootScope", "RecipeSrv","ProductSrv", 
    function($scope, $rootScope, RecipeSrv,ProductSrv) {
      $scope.recipes = [];
      $scope.products = [];
      function updateRecipes(){
        RecipeSrv.getRecipes("").success(function(data){
          $scope.recipes = data;
        });
      }

      function updateProducts(){
        ProductSrv.getProducts("").success(function(data){
          $scope.products = data;
        });
      }

      updateRecipes();
      updateProducts();

      $scope.chooseRecipe=function(id){
        console.log(id);
        RecipeSrv.chooseRecipe(id).success(function(){
          $rootScope.$emit("updateChoosenRecipes",id);
        }).error(function(){
          console.log("failed");
        })
      }

      $scope.rejectRecipe=function(id){
        console.log(id);
        RecipeSrv.rejectRecipe(id).success(function(){
          $rootScope.$emit("updateChoosenRecipes",id);
        })
      }

      $scope.chooseProduct = function(id){
        console.log(id);
        ProductSrv.chooseProduct(id).success(function(){
          $rootScope.$emit("updateChoosenProducts",id);
        });

      }

      $scope.rejectProduct = function(id){
        ProductSrv.rejectProduct(id).success(function(){
          $rootScope.$emit("updateChoosenProducts",id);
        });
      }



      $rootScope.$on("updateChoosenRecipes",function(e,id){
        console.log("hey");
        
          $.each($scope.recipes, function (i, obj) {
            if(obj.Id == id){
              if(obj.IsChoosen){
                obj.IsChoosen = false;
              }else{
                obj.IsChoosen = true;
              }
            }
          })
      })

      $rootScope.$on("updateChoosenProducts",function(e,id){
        $.each($scope.products, function (i,obj) {
          if(obj.Id == id){
            if(obj.IsChoosen){
              obj.IsChoosen = false;
            }else{
              obj.IsChoosen = true;
            }
          }
        })
      });
      
    }]);


