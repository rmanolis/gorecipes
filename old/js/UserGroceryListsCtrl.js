app.controller('UserGroceryListsCtrl',
    ["$scope", "UserSrv",
    function($scope,UserSrv) {
      function getGroceries(){
        UserSrv.getGroceryLists().success(function(lo){
          console.log(lo);
          $scope.groceries = lo.Objects;
        });
      }
      getGroceries();
      $scope.deleteGrocery = function(cookie_id){
        UserSrv.deleteGrocery(cookie_id).success(function(){
          getGroceries();
        });

      }

    }]);


