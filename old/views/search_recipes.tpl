<!-- .master-header -->
<div class="page-banner">
	<div class="container">
		<div class="row">
			<div class="col-sm-7">
				<h2 class="page-title">Recipes</h2>
			</div>
			<div class="col-sm-5">
				<ol class="breadcrumb">
					<li><a href="/">Home</a></li>
					<li class="active">Recipes</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<div ng-controller="SearchRecipesCtrl">
<div class="ui-widget">
Name: <input ng-model="name">
<br/>
Tags: <input id="tags" size="50" ng-model="stags"  >
<br/>
Ingredients: <input id="ingredients" ng-model="singredients" size="50" >
<br/>

<button  ng-click="submit()">Submit</button>
</div>

<div class="ui-widget">
  <ul ng-repeat="recipe in recipes">
    <li class="recipe">
        <a href="{{'/recipes/'+recipe.Id}}" > <strong >{{recipe.Name}}</strong></a>
        <div ng-if="!recipe.IsChoosen">
          <button ng-click="choose(recipe.Id)">Choose</button>
        </div>
        <div ng-if="recipe.IsChoosen">
          <button ng-click="reject(recipe.Id)">Reject</button>
        </div>

    </li>
  </ul>
</div>

<div id="pagination"></div>
</div>


{% template "visitor/basket" . %}

<div class="basket">
{% define "basket" %}
{%end%}
</div>


