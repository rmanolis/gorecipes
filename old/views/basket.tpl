
<div ng-controller="BasketCtrl">
		<h3 class="widget-title heading-bottom-line">Choosen Recipes</h3>
		<ul ng-repeat="recipe in choosenRecipes">
      <li>
         <i class="fa fa-angle-right"></i> <a href="{{'/recipes/'+ recipe.Id }}" > <strong >{{recipe.Name}}</strong></a>
            <button ng-click="rejectRecipe(recipe.Id)">Reject</button>
      </li>
    </ul>
    
</div>

