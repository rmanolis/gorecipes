package main

import (
	"flag"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"gorecipes/confs"
	"gorecipes/models/loader"
	"gorecipes/routes"
	"gorecipes/utils"
	"net/http"
)

func main() {
	router := mux.NewRouter()
	routes.SetRouter(router)
	gdt := utils.NewDataStore(confs.DBName, "127.0.0.1", "27017")
	n := negroni.New(negroni.NewRecovery(), confs.NewLogger(), negroni.NewStatic(http.Dir("public")))
	n.Use(negroni.HandlerFunc(gdt.MgoMiddleware))
	loader.LoadEverything(gdt.Session.Clone().DB(gdt.DBName))
	static := negroni.NewStatic(http.Dir("static"))
	static.Prefix = "/static"
	n.Use(static)
	opts := cors.Options{
		AllowedOrigins:   []string{"*"},
		ExposedHeaders:   []string{"*"},
		AllowedHeaders:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE"},
		AllowCredentials: true,
	}

	handler := cors.New(opts).Handler(router)
	n.UseHandler(handler)
	devPtr := flag.Bool("deploy", false, "is for deployment")
	flag.Parse()
	if *devPtr == false {
		n.Run(":8001")
	} else {
		n.Run(":80")
	}
}
