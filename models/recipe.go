package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

type Recipe struct {
	Id              bson.ObjectId ` bson:"_id,omitempty"`
	Name            string
	Servings        string
	Steps           string
	Tags            []bson.ObjectId
	STags           string
	Ingredients     []bson.ObjectId
	SIngredients    string
	Products        []bson.ObjectId
	Photos          []bson.ObjectId
	IsChoosen       bool //not for saving
	Date            time.Time
	HasPhoto        bool
	IsApproved      bool
	CookTime        time.Time
	PreparationTime time.Time
	Language        Language
	UserId          bson.ObjectId
	UserName        string
}

func (recipe *Recipe) Insert(db *mgo.Database) error {
	c := db.C(RECIPE)
	recipe.Date = time.Now()
	return c.Insert(&recipe)
}

func (recipe *Recipe) Update(db *mgo.Database) error {
	c := db.C(RECIPE)
	return c.UpdateId(recipe.Id, recipe)
}

func (r *Recipe) Remove(db *mgo.Database) error {
	c := db.C(RECIPE)
	return c.RemoveId(r.Id)
}

func GetRecipes(db *mgo.Database, size, page int) (*ListObjects, error) {
	c := db.C(RECIPE)
	recipes := []Recipe{}
	skip := size * page
	err := c.Find(bson.M{}).Sort("-date").Skip(skip).Limit(size).All(&recipes)
	total, _ := c.Find(bson.M{}).Count()
	lo := GetListObjects(size, page, total, recipes)
	return lo, err

}

func GetRecipesByTags(db *mgo.Database, tags []bson.ObjectId, size, page int) ([]Recipe, error) {
	c := db.C(RECIPE)
	recipes := []Recipe{}
	skip := size * page
	err := c.Find(bson.M{"tags": bson.M{"$all": tags}}).
		Skip(skip).Limit(size).All(&recipes)
	return recipes, err
}

func SearchRecipes(db *mgo.Database, name string,
	add_ings,
	rem_ings,
	add_tags,
	rem_tags []bson.ObjectId, show_all bool,
	size, page int) (*ListObjects, error) {
	c := db.C(RECIPE)
	recipes := []Recipe{}
	skip := size * page
	query := bson.M{}
	and := []interface{}{}
	if len(add_ings) > 0 {
		and = append(and, bson.M{"ingredients": bson.M{"$all": add_ings}})
	}
	if len(rem_ings) > 0 {
		and = append(and, bson.M{"ingredients": bson.M{"$nin": rem_ings}})
	}
	if len(add_tags) > 0 {
		and = append(and, bson.M{"tags": bson.M{"$all": add_tags}})
	}
	if len(rem_tags) > 0 {
		and = append(and, bson.M{"tags": bson.M{"$nin": rem_tags}})
	}
	if len(and) > 0 {
		query["$and"] = and
	}
	if len(name) > 0 {
		query["name"] = bson.M{"$regex": name, "$options": "i"}
	}
	if !show_all {
		query["isapproved"] = true
	}
	err := c.Find(query).Sort("-date").
		Skip(skip).Limit(size).All(&recipes)
	total, _ := c.Find(query).Count()
	lo := GetListObjects(size, page, total, recipes)
	return lo, err
}

func contains(ls []bson.ObjectId, e bson.ObjectId) bool {
	for _, a := range ls {
		if a == e {
			return true
		}
	}
	return false
}

func GetIngredientsByRecipes(db *mgo.Database, recids []bson.ObjectId) ([]Ingredient, error) {
	crec := db.C(RECIPE)
	recipes := []Recipe{}
	err := crec.Find(bson.M{"_id": bson.M{"$in": recids}}).All(&recipes)
	if err != nil {
		return nil, err
	}
	ingids := []bson.ObjectId{}
	for _, rec := range recipes {
		for _, ing := range rec.Ingredients {
			if !contains(ingids, ing) {
				ingids = append(ingids, ing)
			}
		}
	}
	cing := db.C(INGREDIENT)
	ingredients := []Ingredient{}
	err = cing.Find(bson.M{"_id": bson.M{"$in": ingids}}).All(&ingredients)
	if err != nil {
		return nil, err
	}
	return ingredients, err
}

func FindRecipes(db *mgo.Database, name string, page, size int) ([]Recipe, error) {
	c := db.C(RECIPE)
	recipes := []Recipe{}
	skip := size * page
	err := c.Find(bson.M{"name": bson.M{"$regex": name}}).Sort("-date").
		Skip(skip).Limit(size).All(&recipes)
	return recipes, err

}

func FindRecipeId(db *mgo.Database, id bson.ObjectId, rec *Recipe) error {
	c := db.C(RECIPE)
	err := c.FindId(id).One(rec)
	return err
}

func (r *Recipe) AddTags(db *mgo.Database, tags []bson.ObjectId) error {
	c := db.C(RECIPE)
	r.Tags = tags
	return c.UpdateId(r.Id, r)
}

func (r *Recipe) AddIngredients(db *mgo.Database, ingredients []bson.ObjectId) error {
	c := db.C(RECIPE)
	r.Ingredients = ingredients
	return c.UpdateId(r.Id, r)
}

func (r *Recipe) RemoveTag(db *mgo.Database, tag bson.ObjectId) error {
	c := db.C(RECIPE)
	for i, _ := range r.Tags {
		if r.Tags[i] == tag {
			r.Tags = append(r.Tags[:i], r.Tags[i+1:]...)
			break
		}
	}
	return c.UpdateId(r.Id, r)
}

func (r *Recipe) RemoveIngredient(db *mgo.Database, ing bson.ObjectId) error {
	c := db.C(RECIPE)
	for i, _ := range r.Tags {
		if r.Ingredients[i] == ing {
			r.Ingredients = append(r.Ingredients[:i], r.Ingredients[i+1:]...)
			break
		}
	}
	return c.UpdateId(r.Id, r)
}

func (r *Recipe) GetTags(db *mgo.Database) []Tag {
	tags := []Tag{}
	for _, v := range r.Tags {
		tag := new(Tag)
		err := FindTagId(db, v, tag)
		if err == nil {
			tags = append(tags, *tag)
		}
	}
	return tags
}

func (r *Recipe) GetIngredients(db *mgo.Database) []Ingredient {
	ings := []Ingredient{}
	for _, v := range r.Ingredients {
		ing := new(Ingredient)
		err := FindIngredientId(db, v, ing)
		if err == nil {
			ings = append(ings, *ing)
		}
	}
	return ings
}
