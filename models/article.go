package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

type Article struct {
	Id       bson.ObjectId ` bson:"_id,omitempty"`
	UserId   bson.ObjectId
	Link     string
	Title    string
	Text     string
	Date     time.Time
	HasPhoto bool
	Language Language
}

func (a *Article) Insert(db *mgo.Database) error {
	c := db.C(ARTICLE)
	a.Id = bson.NewObjectId()
	a.Date = time.Now()
	return c.Insert(a)
}

func (a *Article) Update(db *mgo.Database) error {
	c := db.C(ARTICLE)
	return c.UpdateId(a.Id, a)
}

func (a *Article) Remove(db *mgo.Database) error {
	c := db.C(ARTICLE)
	return c.RemoveId(a.Id)
}

func FindArticleByLink(db *mgo.Database, link string, article *Article) error {
	c := db.C(ARTICLE)
	return c.Find(bson.M{"link": link}).One(&article)
}

func SearchArticles(db *mgo.Database, title, text string,
	size, page int) (*ListObjects, error) {
	c := db.C(ARTICLE)
	arts := []Article{}
	skip := size * page
	query := bson.M{}
	if len(title) > 0 {
		query["title"] = bson.M{"$regex": title, "$options": "i"}
	}
	if len(text) > 0 {
		query["text"] = bson.M{"$regex": text, "$options": "i"}
	}

	err := c.Find(query).
		Skip(skip).Limit(size).All(&arts)
	total, _ := c.Find(query).Count()
	lo := GetListObjects(size, page, total, arts)
	return lo, err
}
