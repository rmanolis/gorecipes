package models

type Language string

const (
	ENGLISH = Language("en")
	GREEK   = Language("el")
)
