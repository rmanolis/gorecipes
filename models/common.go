package models

type ListObjects struct {
	Page    int
	Size    int
	Total   int
	Objects interface{}
}

func GetListObjects(size, page int, total int, objects interface{}) *ListObjects {
	lo := new(ListObjects)
	lo.Total = total
	lo.Page = page
	lo.Size = size
	lo.Objects = objects
	return lo
}
