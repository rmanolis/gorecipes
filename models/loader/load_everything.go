package loader

import (
	"encoding/json"
	"gorecipes/models"

	"fmt"
	"gopkg.in/mgo.v2"
	"io/ioutil"
)

func LoadIngredients(db *mgo.Database, language, file string) {
	b, err := ioutil.ReadFile(file)
	if err != nil {
		panic("no file for ingredients")
	}
	data := []string{}
	json.Unmarshal(b, &data)
	for _, v := range data {
		ing := new(models.Ingredient)
		err := models.FindIngredient(db, v, ing)
		if err != nil {
			ing.Name = v
			ing.Language = models.Language(language)
			ing.Insert(db)
		}
	}
}

func LoadTags(db *mgo.Database, language, file string) {
	b, err := ioutil.ReadFile(file)
	if err != nil {
		panic("no file for tags")
	}
	data := []string{}
	json.Unmarshal(b, &data)
	for _, v := range data {
		tag := new(models.Tag)
		err := models.FindTag(db, v, tag)
		if err != nil {
			tag.Name = v
			tag.Language = models.Language(language)
			tag.Insert(db)
		}
	}
}

func LoadEverything(db *mgo.Database) {
	dir := "data/"
	languages, _ := ioutil.ReadDir("data/")
	for _, l := range languages {
		ing_file := dir + l.Name() + "/ingredients.json"
		fmt.Println(ing_file)
		LoadIngredients(db, l.Name(), ing_file)
		tag_file := dir + l.Name() + "/tags.json"
		fmt.Println(tag_file)
		LoadTags(db, l.Name(), tag_file)
	}
	//models.Ingredient
}
