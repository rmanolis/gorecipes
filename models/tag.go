package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"strings"
)

type Tag struct {
	Id       bson.ObjectId ` bson:"_id,omitempty"`
	Name     string
	Language Language
}

func NamesToTags(db *mgo.Database, names []string) ([]bson.ObjectId, []bson.ObjectId) {
	add_ids := []bson.ObjectId{}
	remove_ids := []bson.ObjectId{}
	for _, v := range names {
		name := v
		is_neg := false
		if strings.HasPrefix(name, "-") {
			name = name[1:]
			is_neg = true
		}
		tag := new(Tag)
		err := FindTag(db, name, tag)
		if err == nil {
			if is_neg {
				remove_ids = append(remove_ids, tag.Id)
			} else {
				add_ids = append(add_ids, tag.Id)
			}
		}
	}
	return add_ids, remove_ids
}

func (tag *Tag) Insert(db *mgo.Database) error {
	c := db.C(TAG)
	tag.Id = bson.NewObjectId()
	return c.Insert(tag)
}

func GetTags(db *mgo.Database, size, page int) (*ListObjects, error) {
	c := db.C(TAG)
	tags := []Tag{}
	skip := size * page
	err := c.Find(bson.M{}).Skip(skip).Limit(size).All(&tags)
	total, _ := c.Find(bson.M{}).Count()
	lo := GetListObjects(size, page, total, tags)
	return lo, err
}

func FindTag(db *mgo.Database, name string, tag *Tag) error {
	c := db.C(TAG)
	err := c.Find(bson.M{"name": name}).One(&tag)
	return err
}

func FindTags(db *mgo.Database, name string, size, page int) ([]Tag, error) {
	c := db.C(TAG)
	tags := []Tag{}
	skip := size * page
	err := c.Find(bson.M{"name": bson.M{"$regex": name, "$options": "i"}}).Skip(skip).Limit(size).All(&tags)
	return tags, err

}

func FindTagId(db *mgo.Database, id bson.ObjectId, tag *Tag) error {
	c := db.C(TAG)
	err := c.FindId(id).One(&tag)
	return err
}

func TagIdExists(db *mgo.Database, id bson.ObjectId) bool {
	c := db.C(TAG)
	n, _ := c.FindId(id).Count()
	return (n > 0)

}

func (tag *Tag) Remove(db *mgo.Database) error {
	c := db.C(TAG)
	return c.RemoveId(tag.Id)
}

func (tag *Tag) Update(db *mgo.Database) error {
	c := db.C(TAG)
	return c.UpdateId(tag.Id, tag)
}

func SearchTags(db *mgo.Database, name string,
	size, page int) (*ListObjects, error) {
	c := db.C(TAG)
	tags := []Tag{}
	skip := size * page
	query := bson.M{}
	if len(name) > 0 {
		query["name"] = bson.M{"$regex": name, "$options": "i"}
	}
	err := c.Find(query).
		Skip(skip).Limit(size).All(&tags)
	total, _ := c.Find(query).Count()
	lo := GetListObjects(size, page, total, tags)
	return lo, err
}
