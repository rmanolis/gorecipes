package utils

import (
	"errors"
	"github.com/gorilla/sessions"
	"gopkg.in/mgo.v2/bson"
	"gorecipes/confs"
	"math/rand"
	"net/http"
	"time"
)

var store = sessions.NewCookieStore([]byte("secret"))

func SetSession(w http.ResponseWriter, r *http.Request, name, value string) {
	session, err := store.Get(r, "session")
	if err != nil {
		confs.LOG.Error.Println(err.Error())
	}

	session.Values[name] = value
	session.Save(r, w)
}

func GetSession(r *http.Request, name string) (string, error) {
	session, err := store.Get(r, "session")
	if err != nil {
		confs.LOG.Error.Println(err.Error())
	}
	cookie, ok := session.Values[name]
	if !ok {
		return "", errors.New("no cookie")
	}
	s, ok := cookie.(string)
	if !ok {
		return "", errors.New("no cookie")
	}
	return s, nil
}

func GetCookieId(r *http.Request) (bson.ObjectId, error) {
	str, err := GetSession(r, "cookie_id")
	if err != nil {
		return "", err
	}
	if !bson.IsObjectIdHex(str) {
		return "", errors.New("no real id")
	}
	return bson.ObjectIdHex(str), nil
}

func GetUserId(r *http.Request) (string, error) {
	return GetSession(r, "user_id")
}

func GetAdminId(r *http.Request) (string, error) {
	return GetSession(r, "admin_id")
}

func DeleteCookies(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r, "session")
	if err != nil {
		confs.LOG.Error.Println(err.Error())
	}

	//session.Options.MaxAge = -1

	delete(session.Values, "user_id")
	delete(session.Values, "admin_id")
	session.Save(r, w)

}

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandSeq(n int) string {
	rand.Seed(time.Now().Unix())
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func Render(w http.ResponseWriter, r *http.Request, tpl string, data map[string]interface{}) {
	if data == nil {
		data = map[string]interface{}{}
	}
	_, err := GetUserId(r)
	if err == nil {
		data["IsUser"] = true
	}

	_, err = GetAdminId(r)
	if err == nil {
		data["IsAdmin"] = true
	}

	confs.Render.HTML(w, http.StatusOK, tpl, data)
}

func NoLayoutRender(w http.ResponseWriter, r *http.Request, tpl string, data map[string]interface{}) {
	confs.NoLayoutRender.HTML(w, http.StatusOK, tpl, data)
}

func AuthUserMiddleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	_, err := GetUserId(r)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	} else {
		next(w, r)
	}
}

func AuthUserHandler(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		_, err := GetUserId(r)
		if err != nil {
			w.WriteHeader(http.StatusUnauthorized)
			return
		} else {
			next(w, r)
		}
	}
}
