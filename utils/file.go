package utils

import (
	"errors"
	"github.com/rakyll/magicmime"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
)

const MAX_MEMORY = 6 * 1024 * 1024

var mimetypes = map[string][]string{
	"image": []string{"image/jpeg"},
	//"image/gif", "image/png"},
	//"sound":  []string{"audio/mpeg", "audio/x-wav", "application/ogg"},
	//"record": []string{"audio/x-wav"},
}

func isCorrectMimeType(ftype string, buf []byte) bool {
	err := magicmime.Open(magicmime.MAGIC_MIME_TYPE | magicmime.MAGIC_SYMLINK | magicmime.MAGIC_ERROR)
	defer magicmime.Close()
	if err != nil {
		panic(err)
	}
	mt, err := magicmime.TypeByBuffer(buf)
	if err != nil {
		return false
	}
	for _, v := range mimetypes[ftype] {
		if v == mt {
			return true
		}
	}
	return false
}

func SaveFile(r *http.Request, path string) error {
	buf_ls, err := Files(r)
	if err != nil {
		return err
	}

	for _, buf := range buf_ls {
		if !isCorrectMimeType("image", buf) {
			return errors.New("filetype is not the correct one")
		}
		err = ioutil.WriteFile(path, buf, os.ModePerm)
		if err != nil {
			return err
		}
	}
	return nil
}

func Files(r *http.Request) ([][]byte, error) {
	form, err := MultiForm(r)
	if len(form.File) == 0 {
		return nil, errors.New("no files to save")
	}
	if err != nil {
		return nil, err
	}
	return ReadFiles(form)
}

func MultiForm(r *http.Request) (*multipart.Form, error) {
	err := r.ParseMultipartForm(MAX_MEMORY)
	return r.MultipartForm, err
}

func ReadFiles(form *multipart.Form) ([][]byte, error) {
	files := form.File
	buf_ls := [][]byte{}
	for _, fileHeaders := range files {
		for _, fileHeader := range fileHeaders {
			file, err := fileHeader.Open()
			if err != nil {
				return nil, err
			}
			buf, err := ioutil.ReadAll(file)
			if err != nil {
				return nil, err
			}
			buf_ls = append(buf_ls, buf)
		}
	}
	return buf_ls, nil
}

func DeleteFile(path string) error {
	err := os.Remove(path)
	return err
}
