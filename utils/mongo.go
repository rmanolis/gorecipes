package utils

import (
	"fmt"
	"net/http"
	"strconv"

	"gorecipes/confs"

	"github.com/gorilla/context"
	"gopkg.in/mgo.v2"
)

func SizePage(r *http.Request) (int, int) {
	vls := r.URL.Query()
	page_str := vls.Get("p")
	page, err := strconv.Atoi(page_str)
	if err != nil {
		page = 1
	}

	size_str := vls.Get("s")
	size, err := strconv.Atoi(size_str)
	if err != nil {
		size = 10
	}
	page = page - 1

	return size, page

}

func GetDB(r *http.Request) *mgo.Database {
	db := context.Get(r, confs.DBName)
	return db.(*mgo.Database)
}

func SetDB(r *http.Request, db *mgo.Database, name string) {
	context.Set(r, name, db)
}

type DataStore struct {
	DBName  string
	Session *mgo.Session
}

func NewDataStore(name, ip, port string) *DataStore {
	serv := fmt.Sprint(ip, ":", port)
	fmt.Println(serv)
	session, err := mgo.Dial(serv)
	if err != nil {
		panic(err)
	}
	dt := new(DataStore)
	dt.Session = session
	dt.DBName = name

	return dt
}

func (dt *DataStore) MgoMiddleware(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	reqSession := dt.Session.Clone()
	defer reqSession.Close()
	db := reqSession.DB(dt.DBName)
	SetDB(r, db, dt.DBName)
	next(rw, r)
}
