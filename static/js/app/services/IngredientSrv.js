app.factory('IngredientSrv',
    ["$http",
    function($http){
  var obj = {};
  
  obj.getIngredients = function(name){
    return $http({
      method: "GET",
      url: "/rest/ingredients?" + "Name="+name,
    });
  }

  obj.addIngredient = function(name, tags,file){
    var fd = new FormData();
    fd.append("file", file);
    fd.append("Name", name);
    fd.append("Tags", JSON.stringify(tags));
    return $.ajax({
      method: "POST",
      url: "/admin/ingredients/add",
      cache: false,
      contentType: false,
      processData: false,
      data: fd,
    })
  }

  obj.editIngredient = function(id, name, tags, file){
    var fd = new FormData();
    fd.append("file", file);
    fd.append("Name", name);
    fd.append("Tags", JSON.stringify(tags));
    return $.ajax({
      method: "PUT",
      url: "/admin/ingredients/"+id,
      cache: false,
      contentType: false,
      processData: false,
      data:fd,    }) 
  }

  obj.deleteIngredient = function(id){
    return $http({
      method: "DELETE",
      url: "/admin/ingredients/"+id,
      }) 
  }


  obj.searchIngredients = function(name, tags, page, size){
    return $http({
      method: "POST",
      url: "/rest/search/ingredients?p="+ page + "&s="+size,
      data: {
        "Name": name ,
        "Tags": tags,
      }
    })

  }


  obj.getIngredient = function(id){
    return $http({
      method: "GET",
      url: "/rest/ingredients/"+id
    }) 
  }

  obj.getIngredientsByChoosenRecipes = function(){
    return $http({
      method: "GET",
      url: "/rest/choosen/recipes/ingredients",
    })
  }

  obj.getIngredientsByRecipe = function(id){
    return $http({
      method: "GET",
      url: "/rest/recipes/"+id+"/ingredients",
    })
  }

  obj.deletePicture = function(id){
    return $http({
      method: "DELETE",
      url: "/admin/ingredients/"+id + "/picture",
    }) 
  }



  return obj;
    }]);
