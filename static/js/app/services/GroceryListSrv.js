app.factory('GroceryListSrv',
    ["$http","StorageSrv", "$log",
    function($http, StorageSrv, $log){
      var obj = {};
      /*
       * GroceryList{
       *  Ingredients: [
       *    {Name: string,
       *     State: string
       *     }
       *  ]
       *  Recipes: [{
       *    Name: string
       *    Id : string
       *  }]
       * */

      /*obj.col = StorageSrv.db.collection("groceries");
      obj.col.load(function(err){
        if(err){
          $log.error(err);
        }
      });*/

      obj.col = StorageSrv.groceries


      obj.addGroceryList = function(note){
        obj.col.put(note)
      }


      obj.getGroceryLists = function(){
        return   obj.col.allDocs({include_docs: true, descending:true});
      }

      obj.getGroceryList = function(id){
        return   obj.col.get(id);
      }

      obj.deleteGroceryList = function(note){
        obj.col.remove(note);
      }

      return obj;

    }])
