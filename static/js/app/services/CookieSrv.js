app.factory('CookieSrv',
    ["$http",
    function($http){
  var obj = {};
  obj.saveCookie = function (name) {
    return $http({
      method: "PUT",
      url: "/rest/cookie/save",
      data:{
        Name:name,
      },
    });
  }

  obj.cancelCookie = function ( ) {
    return $http({
      method: "PUT",
      url: "/rest/cookie/cancel",
    });
  }


  obj.newCookie = function(){
    return $http({
      method: "POST",
      url: "/rest/cookie",
    });

  }

  obj.searchCookies = function(name,is_user,is_finished,page,size){
    return $http({
      method: "POST",
      url: "/admin/search/cookies?p="+ page + "&s="+
        size ,
      data:{
        Name:name,
        IsUser:is_user,
        IsFinished:is_finished,
      }
     });
  }

  obj.getUserCookies = function(id,page,size){
    return $http({
      method: "GET",
      url: "/admin/users/"+id+"/cookies?p="+ page + "&s="+
        size,
    });
  }

  obj.getUserProducts = function(user_id, cookie_id){
    return $http({
      method: "POST",
      url: "/admin/users/"+user_id+"/products",
      data:{
        CookieId:cookie_id
      }
    });
  }

  obj.getUserIngredients = function(user_id,cookie_id){
    return $http({
      method: "POST",
      url: "/admin/users/"+user_id+"/ingredients",
      data:{
        CookieId:cookie_id
      }
    });
  }

  obj.getUserRecipesProducts = function(user_id,cookie_id){
    return $http({
      method: "POST",
      url: "/admin/users/"+user_id+"/recipes/products",
      data:{
        CookieId:cookie_id
      }

    });
  }

  return obj; 
    }])
