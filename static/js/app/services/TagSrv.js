app.factory('TagSrv',
    ["$http",
    function($http){
      var obj = {};

      obj.getTags = function(name){
        return $http({
          method: "GET",
          url: "/rest/tags?" + "Name="+name,
        });
      }

      obj.getTag = function(id){
        return $http({
          method: "GET",
          url: "/rest/tags/" + id,
        });
      }

      obj.searchTags = function(name){
        return $http({
          method: "POST",
          url: "/rest/search/tags",
          data:{
            Name:name,
          }
        });

      }

      obj.addTag = function(name){
        var fd = new FormData();
        fd.append("Name", name);
        return $.ajax({
          method: "POST",
          url: "/admin/tags/add",
          cache: false,
          contentType: false,
          processData: false,
          data: fd,
        })
      }

      obj.editTag = function(id, name){
        var fd = new FormData();
        fd.append("Name", name);
        return $.ajax({
          method: "PUT",
          url: "/admin/tags/"+id,
          cache: false,
          contentType: false,
          processData: false,
          data: fd,
        }) 
      }

      obj.deleteTag = function(id){
        return $http({
          method: "DELETE",
          url: "/admin/tags/"+id,
        }) 
      }





      return obj;
    }]);
