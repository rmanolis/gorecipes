app.factory('HelperSrv',
    [
    function(){
  var obj = {};
  obj.split = function split( val ) {
    return val.split( /,\s*/ );
  }

  obj.extractLast = function extractLast( term ) {
    return obj.split( term ).pop();
  }

  
  obj.createStringFromList= function(list){
      var str ="";
      for(var i=0; i < list.length; i++){
        str += list[i] + ", ";
      }
     
      return str;

    }

  return obj;
    }]);
