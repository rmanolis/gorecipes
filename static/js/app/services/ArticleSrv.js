app.factory('ArticleSrv',
    ["$http",
    function($http){
  var obj = {};
  
  obj.getArticle = function(link){
    return $http.get("/rest/articles/"+link);
  }

  obj.searchArticles = function(title,text,page,size){
    return $http({
      method: "POST",
      url: "/rest/search/articles?p="+page+"&s="+size,
      data: {
        Title: title,
        Text: text,
      },
    });
  }

  obj.addArticle = function(title, link,text,file){
    var fd = new FormData();
    fd.append("file", file);
    fd.append("Title", title);
    fd.append("Link", link);
    fd.append("Text", text);
    return $.ajax({
      method: "POST",
      url: "/admin/articles/add",
      cache: false,
      contentType: false,
      processData: false,
      data: fd,
    })
  }

  obj.editArticle = function(id, title, link,text, file){
    var fd = new FormData();
    fd.append("file", file);
    fd.append("Title", title);
    fd.append("Link", link);
    fd.append("Text", text);
    return $.ajax({
      method: "PUT",
      url: "/admin/articles/"+id,
      cache: false,
      contentType: false,
      processData: false,
      data:fd,    }) 
  }

  obj.deleteArticle = function(id){
    return $http({
      method: "DELETE",
      url: "/admin/articles/"+id,
      }) 
  }

  obj.deletePicture = function(id){
    return $http({
      method: "DELETE",
      url: "/admin/articles/"+id + "/picture",
    }) 
  }

  return obj;
    }]);
