app.factory('UserSrv',
    ["$http",
    function($http){
      var obj = {};
      obj.searchUsers = function(name,email,page,size){
        return $http({
          method: "POST",
          url: "/admin/search/users?p="+ page + "&s="+
            size ,
          data:{
            Name:name,
            Email:email
          }
        });
      }

      obj.getRecipes = function(name,page, size){
        return $http({
          method: "GET",
          url: "/rest/user/recipes?p="+ page + "&s=" + size  + "&Name="+name,
        });
      } 


      obj.isUser = function(){
        return $http({
          method: "GET",
          url: "/rest/auth/user",    
        });

      }

      obj.editPassword = function(pass, repass){
        return $http({
          method: "PUT",
          url: "/rest/user/password",  
          data: {
            Password:pass,
            RepeatPassword:repass,
          },
        });

      }

      obj.addPicture = function(file){
        var fd = new FormData();
        fd.append("file", file);
        return $.ajax({
          method: "POST",
          url: "/rest/user/picture",
          cache: false,
          contentType: false,
          processData: false,
          data: fd,
        })
      }

      obj.login = function(email, password){
        return $http({
          method: "POST",
          url: "/rest/auth/login", 
          data:{
            Email: email,
            Password: password,
          }
        });
      }

      obj.logout = function(email, password){
        return $http({
          method: "GET",
          url: "/rest/auth/logout", 
        });
      }


      return obj;
    }]);
