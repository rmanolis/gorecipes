app.factory('RecipeSrv',
    ["$http",
    function($http){
  var obj = {};
   obj.getRecipes = function(name){
    return $http({
      method: "GET",
      url: "/rest/recipes?" + "Name="+name,
    });
  }

  obj.getRecipe = function(id){
    return $http({
      method: "GET",
      url: "/rest/recipes/"+id
    }) 
  }

  obj.approveRecipe = function(id){
    return $http({
      method: "PUT",
      url: "/admin/recipes/"+id + "/approve",
    }) 
  }

  obj.addRecipe = function(name, servings, steps, tags, 
      ingredients, products,file){
    var fd = new FormData();
    fd.append("Name", name);
    fd.append("Servings", servings);
    fd.append("Steps", steps);
    fd.append("Tags", JSON.stringify(tags));
    fd.append("Ingredients", JSON.stringify(ingredients));
    fd.append("Products", JSON.stringify(products));
    fd.append("file", file);

    return $.ajax({
      method: "POST",
      url: "/user/recipes/add",
      cache: false,
      contentType: false,
      processData: false,
      data: fd,
    })
  }

  obj.editRecipe = function(id, name, servings, steps, 
      tags, ingredients, products,file){
    var fd = new FormData();
    fd.append("Name", name);
    fd.append("Servings", servings);
    fd.append("Steps", steps);
    fd.append("Tags", JSON.stringify(tags));
    fd.append("Ingredients", JSON.stringify(ingredients));
    fd.append("Products", JSON.stringify(products));
    fd.append("file", file);

    return $.ajax({
      method: "PUT",
      url: "/user/recipes/"+id,
      cache: false,
      contentType: false,
      processData: false,
      data: fd,
    }) 
  }

  obj.deleteRecipe = function(id){
    return $http({
      method: "DELETE",
      url: "/user/recipes/"+id,
      }) 
  }

  obj.searchRecipes = function(name, tags, ingredients,show_all, page, size){
    return $http({
      method: "POST",
      url: "/rest/search/recipes?p="+ page + "&s=" + size ,
      data: {
        "Name": name ,
        "Tags": tags,
        "Ingredients": ingredients,
        "ShowAll": show_all,
      }
    })

  }

  obj.getChoosenRecipes =  function(){
    return $http({
      method: "GET",
      url: "/rest/choosen/recipes",
    })

  }
  obj.chooseRecipe = function(id){
    return $http({
      method: "POST",
      url: "/rest/choosen/recipes/" + id,
    })
  }

  obj.rejectRecipe = function(id){
    return $http({
      method: "DELETE",
      url: "/rest/choosen/recipes/" + id ,
    })
  }

  obj.deletePicture = function(id){
    return $http({
      method: "DELETE",
      url: "/user/recipes/"+id + "/picture",
    }) 
  }


  return obj;
    }]);
