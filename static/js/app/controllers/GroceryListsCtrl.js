app.controller('GroceryListsCtrl',
    ["$scope","GroceryListSrv",
    function($scope,GroceryListSrv){
    $scope.grocery_lists = [];            
    function getGroceries(){
        GroceryListSrv.getGroceryLists().then(function(objs){
          console.log(objs.rows);
          $scope.grocery_lists = objs.rows;
          $scope.$apply();
        });
      }
      getGroceries();
      $scope.deleteList = function(value){
        console.log(value);
        GroceryListSrv.deleteGroceryList(value.doc);
        getGroceries();  
      }
      $scope.groceries= null;
      $scope.showList = function(value){
        $scope.groceries = value.doc;
        console.log(value.doc);
      }
    }])
