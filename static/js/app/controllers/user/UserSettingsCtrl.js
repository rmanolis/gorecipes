app.controller('UserSettingsCtrl',
    ["$scope","UserSrv",'Upload', '$timeout', 
    function($scope,UserSrv, Upload, $timeout) {
      $scope.password = "";
      $scope.repeat_password = "";
      $scope.submit = function() {
        UserSrv.editPassword($scope.password,
            $scope.repeat_password).success(function(){
          alert("Password successfully saved");
        }).error(function(d,msg){
              alert("wrong password");
            })
      }

      $scope.fileUpload = function( e)
      {
        var file    = e.target.files[0];
        UserSrv.addPicture(file).success(function(){
          console.log("Picture added");
        })
      }

    }]);

