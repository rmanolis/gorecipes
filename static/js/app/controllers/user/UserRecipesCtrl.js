app.controller('UserRecipesCtrl',
    ["$scope","$location", "UserSrv", 
    function($scope,$location,UserSrv) {
     $scope.sizeList = 20;
     $scope.name = "";
     $scope.recipes  = [];
     $scope.tags =[];
    $scope.ingredients =[]; 
    var createPagination = function(name ,lo){
      $('#pagination').bootpag({
        total: Math.ceil(lo.Total/lo.Size),
        page: lo.Page + 1, 
        firstLastUse: true,
        first: '←',
        last: '→',
        wrapClass: 'pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first'
      }).on("page", function(event, num){
        console.log(num);
        UserSrv.getRecipes(name,num,$scope.sizeList).success(function(lo){
          console.log(lo);
          $scope.recipes = lo.Objects;
        });
      });
    }

     $scope.submit = function() {
      var name = $scope.name;
      console.log(name);
      UserSrv.getRecipes(name,1,$scope.sizeList).success(function(lo){
        console.log(lo);
        $scope.recipes = lo.Objects;
        createPagination(name ,lo);
      }).error(function(){
        console.log("fails to receive ");
      }); 
    }

    $scope.submit();

    $scope.delete = function(id){
      UserSrv.deleteRecipe(id).success(function(){
        $scope.submit();
      });
    }

    $scope.edit = function(id){
      console.log(id);
      $location.path("/user/recipes/"+id);
    }

 
    }]);

