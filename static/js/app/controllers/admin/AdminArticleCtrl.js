app.controller('AdminArticleCtrl',
    ["$scope","ArticleSrv",
    function($scope,ArticleSrv){
    var pname = window.location.pathname;
    var id = pname.split('/').pop();
    $scope.article = {};
       if(id != "add"){ 
        ArticleSrv.getArticle(id).success(function(art){
          $scope.article = art;
        });
    }

    $scope.deletePicture = function(){
     ArticleSrv.deletePicture($scope.article.Link).success(function(){
      $scope.article.HasPhoto = false;
      alert("Picture deleted")
     })
    }
    

    $scope.submit = function() {
        if(id === "add"){
          ArticleSrv.addArticle($scope.article.Title, 
              $scope.article.Link,
              $scope.article.Text,
              $scope.file).done(function(){
            console.log("done!");
            window.location.pathname = "/admin/articles";
          }).fail(function(){
                console.log("failed");
              });
        }else{
          ArticleSrv.editArticle(id,$scope.article.Title, 
              $scope.article.Link,
              $scope.article.Text,
              $scope.file).done(function(){
            console.log("done!");
            window.location.pathname = "/admin/articles";
          }).fail(function(){
                console.log("failed");
              });

        }
      }

      $scope.fileUpload = function( e)
      {
        var file    = e.target.files[0];
        $scope.file = file;    
      }

    

    
    }])
