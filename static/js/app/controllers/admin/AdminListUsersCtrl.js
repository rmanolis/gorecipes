app.controller('AdminListUsersCtrl',
    ["$scope","UserSrv", 
    function($scope,UserSrv) {
      $scope.sizeList = 20;
    $scope.name = "";
    $scope.email =  "";
    $scope.users = [];

    $scope.createPagination = function(name,email,lo){
      $('#pagination').bootpag({
        total: Math.ceil(lo.Total/lo.Size),
        page: lo.Page + 1, 
        firstLastUse: true,
        first: '←',
        last: '→',
        wrapClass: 'pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first'
      }).on("page", function(event, num){
        console.log(num);
        UserSrv.searchUsers(name,email,num,$scope.sizeList).success(function(lo){
          console.log(lo);
          $scope.users = lo.Objects;
        });
      });
    }
    $scope.submit = function() {
      var name = $scope.name;
      var email = $scope.email;
      UserSrv.searchUsers(name,email,1,$scope.sizeList).success(function(lo){
        console.log(lo);
        $scope.users = lo.Objects;
        $scope.createPagination(name,email,lo);
      }).error(function(){
        console.log("fails to receive ");
      }); 
    }

    $scope.submit();

    }]);

