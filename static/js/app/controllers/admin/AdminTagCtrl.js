app.controller('AdminTagCtrl',
    ["$scope","TagSrv", 
    function($scope, TagSrv) {
      var pname = window.location.pathname;
      var id = pname.split('/').pop();
      console.log(id);

      $scope.name = "";
      if(id != "add"){ 
        TagSrv.getTag(id).success(function(tag){
          console.log(tag);
          $scope.name = tag.Name;
        });
      }

      $scope.submit= function(name){
        if(id === "add"){
          TagSrv.addTag(name).success(function(){
            window.location.pathname = "/admin/tags";
          }).error(function(){
            console.log("failed")
          })
        }else{
          TagSrv.editTag(id, name).success(function(){
            window.location.pathname = "/admin/tags";
          }).error(function(){
            console.log("failed")
          })

        }
      }

      $scope.cancel = function(){
        window.location.pathname = "/admin/tags";      
      }
    }])
