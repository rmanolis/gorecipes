app.controller('AdminListTagsCtrl',
    ["$scope","TagSrv", 
    function($scope, TagSrv) {
      $scope.tags = [];
      $scope.name = "";
      $scope.sizeList = 20;
      var createPagination = function(name ,lo){
        $('#pagination').bootpag({
          total: Math.ceil(lo.Total/lo.Size),
          page: lo.Page + 1, 
          firstLastUse: true,
          first: '←',
          last: '→',
          wrapClass: 'pagination',
          activeClass: 'active',
          disabledClass: 'disabled',
          nextClass: 'next',
          prevClass: 'prev',
          lastClass: 'last',
          firstClass: 'first'
        }).on("page", function(event, num){
          console.log(num);
          TagSrv.searchTags(name,num,$scope.sizeList).success(function(lo){
            console.log(lo);
            $scope.tags = lo.Objects;
          });
        });
      }

      $scope.delete = function(id){
        TagSrv.deleteTag(id).success(function(){
          $scope.submit();
        });
      }


      $scope.submit = function(name){
        TagSrv.searchTags(name,1,$scope.sizeList).success(function(lo){
          console.log(lo);
          $scope.tags = lo.Objects;
          createPagination(name,lo);
        }).error(function(){
          console.log("fails to receive ");
        }); 
      }
      $scope.submit();

   



}]);

