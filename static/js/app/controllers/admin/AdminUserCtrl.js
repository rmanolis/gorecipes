app.controller('AdminUserCtrl',
    ["$scope","CookieSrv", 
    function($scope,CookieSrv) {

      $scope.cookies = [];
      $scope.ingredients = [];
      $scope.products = [];
      $scope.recipeProducts = [];
      $scope.createPagination = function(id ,lo){
        $('#pagination').bootpag({
          total: Math.ceil(lo.Total/lo.Size),
          page: lo.Page + 1, 
          firstLastUse: true,
          first: '←',
          last: '→',
          wrapClass: 'pagination',
          activeClass: 'active',
          disabledClass: 'disabled',
          nextClass: 'next',
          prevClass: 'prev',
          lastClass: 'last',
          firstClass: 'first'
        }).on("page", function(event, num){
          console.log(num);
          CookieSrv.getUserCookies(id).success(function(lo){
            $scope.cookies = lo.Objects;
            $scope.createPagination(id, lo);
          });
        });
      }
      var uid =  window.location.pathname.replace("/admin/users/", "")
        CookieSrv.getUserCookies(uid, 1, 20).success(function(lo){
          console.log(lo);
          $scope.cookies=lo.Objects;
          $scope.createPagination(uid,lo);
        })
      $scope.chooseCookie = function(id){
        console.log(id);
        CookieSrv.getUserProducts(uid, id).success(function(products){
          $scope.products = products;
        });
        CookieSrv.getUserIngredients(uid, id).success(function(ings){
          $scope.ingredients = ings;
        });
        CookieSrv.getUserRecipesProducts(uid, id).success(function(products){
          $scope.recipeProducts = products;
        });
      }




    }]);

