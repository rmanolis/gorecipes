app.controller('AdminListArticlesCtrl',
    ["$scope", "ArticleSrv",
    function($scope,ArticleSrv){
    $scope.articles = [];
    $scope.title = "";
    $scope.text = "";
    $scope.sizeList = 20;
    var createPagination = function(title,text ,lo){
      $('#pagination').bootpag({
        total: Math.ceil(lo.Total/lo.Size),
        page: lo.Page + 1, 
        firstLastUse: true,
        first: '←',
        last: '→',
        wrapClass: 'pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first'
      }).on("page", function(event, num){
        console.log(num);
        ArticleSrv.searchArticles(title,text,num,$scope.sizeList).success(function(lo){
          console.log(lo);
          $scope.articles = lo.Objects;
        });
      });
    }

     $scope.delete = function(id,title,text){
        ArticleSrv.deleteArticle(id).success(function(){
          $scope.submit(title,text);
        });
      }

        
    $scope.submit = function(title,text){
      ArticleSrv.searchArticles(title,text,1,$scope.sizeList).success(function(lo){
          console.log(lo);
          $scope.articles = lo.Objects;
          createPagination(title,text,lo);
        }).error(function(){
        console.log("fails to receive ");
      }); 

    }
    $scope.submit();

    }])
