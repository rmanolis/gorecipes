app.controller('AdminStatisticsCtrl',
    ["$scope", 
    function($scope) {

      $scope.start=moment().subtract(10,'days').format("DD/MM/YYYY");
      $scope.end =moment().format("DD/MM/YYYY");


      function getStatistics(start,end,on){
        window.location.href="/admin/search/statistics?Start="+start+"&End="+end + "&On=" +on;
      }

      $scope.getRecipes = function(){
        getStatistics($scope.start,$scope.end,"recipes");
      }
      $scope.getProducts = function(){
        getStatistics($scope.start,$scope.end,"products");
      }


    }]);

