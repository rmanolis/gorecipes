app.controller('RecipeCtrl',
    ["$scope","$rootScope","RecipeSrv", 
    function($scope,$rootScope,RecipeSrv) {
     $scope.id= window.location.pathname.replace("/recipes/","");
    console.log($scope.id);
    $scope.recipe = {};
    function updateRecipe(){
      RecipeSrv.getRecipe($scope.id).success(function(data){
        $scope.recipe = data;
        console.log(data);
      })
    }
    updateRecipe();
    $scope.choose = function(){
     RecipeSrv.chooseRecipe($scope.id).success(function(){
        $rootScope.$emit("updateChoosenRecipes",$scope.id);
      })
    }

    $scope.reject = function(){
       RecipeSrv.rejectRecipe($scope.id).success(function(){
        $rootScope.$emit("updateChoosenRecipes",$scope.id);
       })
    }

    $rootScope.$on("updateChoosenRecipes",function(){
      updateRecipe();
    })

 
    }]);

