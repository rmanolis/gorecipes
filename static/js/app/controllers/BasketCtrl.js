app.controller('BasketCtrl',
    ["$scope","$rootScope","$uibModal","RecipeSrv", "IngredientSrv","GroceryListSrv","CookieSrv",
    function($scope, $rootScope,$uibModal, RecipeSrv,IngredientSrv,GroceryListSrv,CookieSrv ) {
      $scope.choosenRecipes = [];
      $scope.ingredients  = [];
      
      function getChoosenRecipes(){
        RecipeSrv.getChoosenRecipes().success(function(crs){
          $scope.choosenRecipes = crs;
          IngredientSrv.getIngredientsByChoosenRecipes().success(function(ings){
            $scope.ingredients = ings;
          });
        });
      }
      getChoosenRecipes();

      $scope.rejectRecipe = function(id){
        RecipeSrv.rejectRecipe(id).success(function(){
          console.log("rejected");        
          $rootScope.$emit("updateChoosenRecipe",id);
        });
      }

      $scope.save = function(){
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'grocerySaveContent.html',
            controller: 'GrocerySaveCtrl',
            size: 'sm',
            resolve: {
            }
          });
        modalInstance.result.then(function (name) {
          CookieSrv.saveCookie(name).success(function(id){
            console.log("saves " + id);
            var ings = [];
            angular.forEach( $scope.ingredients, function(ing){
              ings.push(ing.Name)
            })
            var recipes = [];
            angular.forEach( $scope.choosenRecipes, function(rec){
              console.log(rec);
              recipes.push({
                Id:rec.Id,
                Name:rec.Name,
                
                HasPhoto : rec.HasPhoto,
                UserName : rec.UserName,
                Date: rec.Date})
            })

            var note ={
              _id:id,
              Name:name,
              Ingredients:ings,
              Recipes: recipes,
            }

            GroceryListSrv.addGroceryList(note);
            $rootScope.$emit("reloadChoosenRecipes");
          })
         }, function () {
         });
        
        }

      $scope.cancel = function(){
        CookieSrv.cancelCookie().success(function(){
            $rootScope.$emit("reloadChoosenRecipes");                      
        });
      }

      $rootScope.$on("updateChoosenRecipe",function(e,id){
        getChoosenRecipes();
      })
      $rootScope.$on("reloadChoosenRecipes",function(e){
        getChoosenRecipes();
      })


    }]);

app.controller('GrocerySaveCtrl', function ($scope, $uibModalInstance) {

  $scope.ok = function () {
    $uibModalInstance.close($scope.name);
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});
