package routes

import (
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
	"gorecipes/ctrls/admin"
	"gorecipes/utils"
)

func adminRoutes(router *mux.Router) {
	art := router.PathPrefix("/admin").Handler(negroni.New(
		negroni.HandlerFunc(utils.AuthUserMiddleware),
	)).Subrouter()
	art.HandleFunc("/tags", admin.ShowTags).Methods("GET")
	art.HandleFunc("/tags/{id}", admin.ShowEditTag).Methods("GET")
	art.HandleFunc("/tags/{id}", admin.AddTag).Methods("POST")
	art.HandleFunc("/tags/{id}", admin.EditTag).Methods("PUT")
	art.HandleFunc("/tags/{id}", admin.DeleteTag).Methods("DELETE")

	art.HandleFunc("/articles", admin.ShowArticles).Methods("GET")
	art.HandleFunc("/articles/{id}", admin.ShowEditArticle).Methods("GET")
	art.HandleFunc("/articles/{id}", admin.AddArticle).Methods("POST")
	art.HandleFunc("/articles/{id}", admin.EditArticle).Methods("PUT")
	art.HandleFunc("/articles/{id}", admin.DeleteArticle).Methods("DELETE")
	art.HandleFunc("/articles/{id}/picture", admin.DeleteFileArticle).Methods("DELETE")

	art.HandleFunc("/ingredients", admin.ShowIngredients).Methods("GET")
	art.HandleFunc("/ingredients/{id}", admin.ShowEditIngredient).Methods("GET")
	art.HandleFunc("/ingredients/{id}", admin.AddIngredient).Methods("POST")
	art.HandleFunc("/ingredients/{id}", admin.EditIngredient).Methods("PUT")
	art.HandleFunc("/ingredients/{id}", admin.DeleteIngredient).Methods("DELETE")
	art.HandleFunc("/ingredients/{id}/picture", admin.DeleteFileIngredient).Methods("DELETE")

	art.HandleFunc("/recipes", admin.ShowRecipes).Methods("GET")
	art.HandleFunc("/recipes/{id}", admin.ShowEditRecipe).Methods("GET")
	art.HandleFunc("/recipes/{id}/approve", admin.ApproveRecipe).Methods("PUT")

	art.HandleFunc("/products", admin.ShowProducts).Methods("GET")
	art.HandleFunc("/products/{id}", admin.ShowEditProduct).Methods("GET")
	art.HandleFunc("/products/{id}", admin.AddProduct).Methods("POST")
	art.HandleFunc("/products/{id}", admin.EditProduct).Methods("PUT")
	art.HandleFunc("/products/{id}", admin.DeleteProduct).Methods("DELETE")
	art.HandleFunc("/products/{id}/picture", admin.DeleteFileProduct).Methods("DELETE")

	art.HandleFunc("/users", admin.ShowUsers).Methods("GET")
	art.HandleFunc("/users/{id}", admin.ShowOneUser).Methods("GET")
	art.HandleFunc("/users/{id}/cookies", admin.ListCookiesOfUser).Methods("GET")
	art.HandleFunc("/search/users", admin.SearchUsers).Methods("POST")

	art.HandleFunc("/statistics", admin.ShowStatistics).Methods("GET")
	art.HandleFunc("/search/statistics", admin.SearchStatistics).Methods("GET")

}
