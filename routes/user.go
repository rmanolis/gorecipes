package routes

import (
	"gorecipes/ctrls/admin"
	"gorecipes/ctrls/viewers"
	"gorecipes/utils"

	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func userRoutes(router *mux.Router) {
	user := router.PathPrefix("/user").Handler(negroni.New(
		negroni.HandlerFunc(utils.AuthUserMiddleware),
	)).Subrouter()
	user.HandleFunc("/settings", viewers.ShowUserSettings).Methods("GET")
	user.HandleFunc("/recipes", viewers.ShowMyRecipes).Methods("GET")
	user.HandleFunc("/recipes/submit", viewers.ShowSubmitRecipe).Methods("GET")
	user.HandleFunc("/recipes/{id}", viewers.ShowSubmitRecipe).Methods("GET")
	user.HandleFunc("/recipes/{id}", admin.AddRecipe).Methods("POST")
	user.HandleFunc("/recipes/{id}", admin.EditRecipe).Methods("PUT")
	user.HandleFunc("/recipes/{id}", admin.DeleteRecipe).Methods("DELETE")
	user.HandleFunc("/recipes/{id}/picture", admin.DeleteFileRecipe).Methods("DELETE")

}
