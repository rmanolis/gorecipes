package routes

import (
	"gorecipes/ctrls/admin"
	"gorecipes/ctrls/rest"

	"gorecipes/utils"

	"github.com/gorilla/mux"
)

func restRoutes(router *mux.Router) {
	rst := router.PathPrefix("/rest").Subrouter()

	rst.HandleFunc("/tags", rest.ListTags)
	rst.HandleFunc("/tags/{id}", rest.GetTag)
	rst.HandleFunc("/search/tags", rest.SearchTags)
	rst.HandleFunc("/ingredients", rest.ListIngredients)
	rst.HandleFunc("/search/ingredients", rest.SearchIngredients).Methods("POST")
	rst.HandleFunc("/ingredients/{id}", rest.GetIngredient)
	rst.HandleFunc("/ingredients/{id}/products", rest.GetProductsByIngredient)

	rst.HandleFunc("/search/recipes", rest.SearchRecipes).Methods("POST")
	rst.HandleFunc("/recipes", rest.ListRecipes)
	rst.HandleFunc("/recipes/{id}", rest.GetRecipe)
	rst.HandleFunc("/recipes/{id}/ingredients", rest.GetIngredientsByRecipe)

	rst.HandleFunc("/choosen/recipes", rest.GetChoosenRecipes)
	rst.HandleFunc("/choosen/recipes/products", rest.GetProductsByChoosenRecipes)
	rst.HandleFunc("/choosen/recipes/ingredients", rest.GetIngredientsByChoosenRecipes)
	rst.HandleFunc("/choosen/recipes/{id}", rest.ChooseRecipe).Methods("POST")
	rst.HandleFunc("/choosen/recipes/{id}", rest.RejectRecipe).Methods("DELETE")

	rst.HandleFunc("/choosen/products", rest.GetChoosenProducts)
	rst.HandleFunc("/choosen/products/{id}", rest.ChooseProduct).Methods("POST")
	rst.HandleFunc("/choosen/products/{id}", rest.RejectProduct).Methods("DELETE")

	rst.HandleFunc("/search/products", rest.SearchProducts).Methods("POST")
	rst.HandleFunc("/products", rest.ListProducts)
	rst.HandleFunc("/products/{id}", rest.GetProduct)

	rst.HandleFunc("/cookie", rest.GetCookie).Methods("POST")
	rst.HandleFunc("/cookie/save", rest.SaveCookie).Methods("PUT")
	rst.HandleFunc("/cookie/cancel", rest.CancelCookie).Methods("PUT")

	rst.HandleFunc("/search/articles", rest.SearchArticles).Methods("POST")
	rst.HandleFunc("/articles/{link}", rest.GetArticle).Methods("GET")
	auth := rst.PathPrefix("/auth").Subrouter()
	auth.HandleFunc("/user", rest.IsUser)
	auth.HandleFunc("/login", rest.Login).Methods("POST")
	auth.HandleFunc("/logout", utils.AuthUserHandler(rest.Logout)).Methods("GET")

	user := rst.PathPrefix("/user").Subrouter()
	user.HandleFunc("/info", rest.AuthUserHandler(rest.UserInfo)).Methods("GET")
	user.HandleFunc("/password", rest.AuthUserHandler(rest.UserEditPassword)).Methods("PUT")
	user.HandleFunc("/picture", rest.AuthUserHandler(rest.UserAddPicture)).Methods("POST")
	user.HandleFunc("/picture", rest.AuthUserHandler(rest.UserDeletePicture)).Methods("DELETE")
	user.HandleFunc("/recipes", rest.AuthUserHandler(admin.ListRecipesByUser)).Methods("GET")

}
