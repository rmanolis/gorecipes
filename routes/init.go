package routes

import (
	"gorecipes/ctrls/viewers"
	"gorecipes/utils"

	"github.com/gorilla/mux"
)

func SetRouter(router *mux.Router) {
	router.HandleFunc("/", viewers.ShowIndex).Methods("GET")
	router.HandleFunc("/auth/login", viewers.ShowLogin).Methods("GET")
	router.HandleFunc("/auth/login", viewers.PostLogin).Methods("POST")
	router.HandleFunc("/auth/logout", utils.AuthUserHandler(viewers.ShowLogout)).Methods("GET")
	router.HandleFunc("/auth/register", viewers.ShowRegister).Methods("GET")
	router.HandleFunc("/auth/register", viewers.PostRegister).Methods("POST")
	router.HandleFunc("/auth/logout", utils.AuthUserHandler(viewers.ShowLogout)).Methods("GET")

	router.HandleFunc("/recipes", viewers.ShowRecipes).Methods("GET")
	router.HandleFunc("/recipes/{id}", viewers.ShowRecipe).Methods("GET")
	router.HandleFunc("/grocery_lists", viewers.ShowGroceryLists).Methods("GET")

	router.HandleFunc("/articles/{link}", viewers.ShowArticle).Methods("GET")

	userRoutes(router)
	adminRoutes(router)
	restRoutes(router)
}
